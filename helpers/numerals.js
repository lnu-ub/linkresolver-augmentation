var Numerals = function() {};

Numerals.prototype.to_roman = function(num) {
    var result = '';
    var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    var roman = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];

    for (var i = 0;i<=decimal.length;i++) {
        while (num%decimal[i] < num) {   
            result += roman[i];
            num -= decimal[i];
        }
    }
    
    return result;
}

Numerals.prototype.from_roman = function(str) {
    var roman_num = str.toUpperCase();
    var result = 0;
    var decimal = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    var roman = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];
    
    for (var i = 0;i<=decimal.length;i++) {
        while (roman_num.indexOf(roman[i]) === 0){
            result += decimal[i];
            roman_num = roman_num.replace(roman[i],'');
        }
    }

    return result;
}

module.exports = new Numerals();
