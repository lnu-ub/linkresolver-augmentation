/* jshint esversion: 6 */
/*************************************************/
const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-uresolver',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });
const crossref = require('../crossref.js');

log.debug('uresolver is in '+context+' context');
/*************************************************/
var confFileName = '../config/'+context+'.js';

log.debug('reading config from '+confFileName);

/* As the default config is used in different contexts the cache
   has to be cleared */
if ( "default" === context ) {
  delete require.cache[require.resolve(confFileName)];
}
var conf = require(confFileName).completion;
//----

var Uresolver = function() {
};

Uresolver.prototype.get_options = function(openurl) {
  var confFileName = '../config/'+context+'.js';
  delete require.cache[require.resolve(confFileName)];
  var conf = require(confFileName).uresolver;
  log.debug('uresolver reading config from '+confFileName);

  var options = this.req_options;
  options.path = conf.path+'?svc_dat=CTO&'+openurl;

  return options;
};

Uresolver.prototype.service_exist = function(content) {
  if ( "object" === typeof content.context_services[0] ) {
    return true;
  }
  else {
    return false;
  }
};

Uresolver.prototype.CTO_to_JSON = function(cto) {
  return this.keys_to_JSON(cto[0].keys[0].key);
};

Uresolver.prototype.CTO_to_OpenURL = function(cto) {
  var parsed_cto = JSON.parse(this.CTO_to_JSON(cto));

  return this.parsed_CTO_to_OpenURL(parsed_cto);
};

Uresolver.prototype.parsed_CTO_to_OpenURL = function(parsed_cto) {
  var str = '';

  for (var key in parsed_cto) {
    if (parsed_cto[key] != '' && !key.startsWith('sfx')) {
      str += `${key}=${encodeURIComponent(parsed_cto[key])}&`;
    }
  }

  str = str.substring(0, str.length-1);
    
  return str;
};

Uresolver.prototype.services_to_JSON = function(context_services) {
  var that = this,
      services = [];

  // context_service är en array av <context_service>-objekt
  context_services[0].context_service.forEach(function(service) {
    services.push( JSON.parse(that.context_service_to_JSON(service)) );
  });

  return services;
};

Uresolver.prototype.context_service_to_JSON = function(context_service) {
  var that = this,
      service = {};

  Object.keys(context_service).forEach(function(key) {
    if( context_service[key].toString().indexOf('[object') === -1 ) {
      service[key] = context_service[key].toString();
    }
    else {
      if ( isNaN(Object.keys(context_service[key])) ) {
        Object.keys(context_service[key]).forEach(function(k) {
          service[k] = context_service[key][k];
        });
      }
      else if ( key === "keys" ) {
        service.key = JSON.parse(that.keys_to_JSON(context_service[key][0].key));
      }
    }
  });

  return JSON.stringify(service);
};

Uresolver.prototype.keys_to_JSON = function(keys) {
  // key är en array av <key>-objekt
  var parsed_cto = {};

  var i = 0;
  while ( i < keys.length ) {

    if ( "undefined" === typeof parsed_cto[keys[i].$.id] ) {
      parsed_cto[keys[i].$.id] = keys[i]._;
    }
    else {
      if ( "string" === typeof parsed_cto[keys[i].$.id] ) {
        // Avoid duplicate values
        if (parsed_cto[keys[i].$.id].indexOf(keys[i]._) === 0) {
          log.debug('Duplicate key values in CTO, not parsing the doublet');
          console.log(parsed_cto[keys[i].$.id]);
          console.log(keys[i]._);
        }
        else {
          let arr = [];
          arr.push(parsed_cto[keys[i].$.id]);
          arr.push(keys[i]._);
          parsed_cto[keys[i].$.id] = arr;
        }
      }
      else if ( Array.isArray(parsed_cto[keys[i].$.id]) ){
        parsed_cto[keys[i].$.id].push(keys[i]._);
      }
    }
    i++;
  }

  return JSON.stringify(parsed_cto);
};

Uresolver.prototype.get_identifier_from_CTO = function(cto, identifier) {
  var myCTO = JSON.parse(this.CTO_to_JSON(cto));

  var id = '';
  var re = new RegExp(`^${identifier}:(.*)$`);

  if (Array.isArray(myCTO.rft_id)) {
    id = myCTO.rft_id.find(function(element) {
      return element.toLowerCase().startsWith(`${identifier}:`);
    });
    
    id = id ? id.match(re)[1] : null;
  }
  else if ("string" === typeof myCTO.rft_id && myCTO.rft_id.toLowerCase().startsWith(`${identifier}:`)) {
    id = myCTO.rft_id.toLowerCase().match(re)[1];
  }

  return id;
};

Uresolver.prototype.req_options = {
  hostname: 'gslg-lnu.userservices.exlibrisgroup.com',
  method: 'GET'    
};

Uresolver.prototype.service = {
  is_free: function(service) {
    return 1 === Number(service.key.Is_free) && !('Filtered' in service.key);
  },
  is_our: function(service) {
    const { key, target_url } = service;

    log.debug(`Checking ${key['package_display_name']}`);
    
    if ( target_url ) {
      if (key.proxy_enabled != 'false' && key.proxy_selected && key.proxy_selected != conf.proxyName ) {
        return false;
      }
    
      log.debug('choice based on target_url');
      return true;
    }
    
    if( !('Filtered' in service.key) ) {
      log.debug('choice based on no filter');
      return true
    }
    
    if ( service.key['Filter reason'] === "Date Filter" ) {
      log.debug('choice based on date filter');
      return true; // There's no target_url if the resource is filtered
    }
    
    log.debug('not choosen');
    return false;
  },
  is_gin: function(service) {
    return "CCC::GIN" === service.key['parser_program'];
  },
  is_other: function(service) {
    return service.key['Filter reason'] != "Date Filter" && "CCC::GIN" != service.key['parser_program'];
  },
  is_preferred: function(service) {
    return  service.key['preferred_link'] == 'true';
  }
};

module.exports = new Uresolver();
