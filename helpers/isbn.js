var Isbn = function() {};

Isbn.prototype.isbn13_valid = function(isbn13) {
    if ( isbn13.length != 13 ) {
        return false;
    }

    let numbers = [];

    for ( let i = 0; i < isbn13.length - 1; i++ ) {
        / [0] * 1, [1] *3, 2 * 1 /
        if ( 0 == i % 2 ) {
            numbers.push(isbn13[i]);
        }
        else {
            numbers.push(isbn13[i] * 3);
        }
    }

    let sum = numbers.reduce( ( previousValue, currentValue ) => previousValue + currentValue );

    if ( 0 == sum  / 10 ) {
        return true;
    }
    else {
        return false;
    }
}

module.exports = new Isbn();
