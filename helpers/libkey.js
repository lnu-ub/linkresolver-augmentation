/* jshint esverion: 6 */
/*************************************************/
const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-libkey',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });
const https = require('https');
const concat = require('concat-stream');

log.debug('libkey is in '+context+' context');
/*************************************************/
var confFileName = '../config/'+context+'.js';

log.debug('reading config from '+confFileName);

/* As the default config is used in different contexts the cache
   has to be cleared */
if ( "default" === context ) {
    delete require.cache[require.resolve(confFileName)];
}

var conf = require(confFileName).libkey;

var Libkey = function() {};

Libkey.prototype.getRetractionNoticeUrl = function( doi ) {
    return new Promise((resolve, reject) => {
        try {
            if ( !conf.token ) {
                throw 'No Libkey token';
            }
        }
        catch ( e ) {
            reject( e );
        }

            
        const options = {
            hostname: conf.host
        };

        options.path = conf.path + doi + `?access_token=${conf.token}`;
        var req = https.request(options, (libkey_res) => {
            libkey_res.pipe(concat(function(buffer) {
                try {
                    const data = JSON.parse(buffer.toString());
                    if ( data.data && data.data.retractionNoticeUrl ) {
                        resolve(data.data.retractionNoticeUrl);
                    }
                    else {
                        reject( data );
                    }
                }
                catch ( e ) {
                    reject( e );
                }
            }));
        });

        req.on('error', (e) => {
            reject(e);
        });
            
        req.end();
    });
};

module.exports = new Libkey();

