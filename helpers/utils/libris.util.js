const https = require('https');
const parseString = require('xml2js').parseString;

class LibrisXSearch {
    constructor(db) {
        this.options = {
            hostname: 'libris.kb.se',
            path: '/xsearch?'
        };
        this.queryObj = {
            database: db
        };
    }
    get database() {
        return this.queryObj.database;
    }
    getFulltextLinks(q) {
        let links = [];
        return new Promise((resolve, reject) => {
            this.search(q)
                .then( (record) => {
                    record.datafield.forEach( (df) => {
                        if ( '856' === df.$.tag ) {
                            let link = {
                                relevance: 0
                            };

                            df.subfield.forEach( (sf) => {
                                if ( 'u' == sf.$.code )
                                    link.url = sf._;
                                if ( sf._.toLowerCase().endsWith('.pdf')  )
                                    link.relevance++;
                                if ( 'primary' == sf._.toLowerCase() )
                                    link.relevance++;
                                if ( 'fulltext' == sf._.toLowerCase() )
                                    link.relevance++;
                            });

                            if ( link.relevance > 0 )
                                links.push(link);
                        }
                    });

                    // Sort the links array based on relevance in descending order
                    links.sort((a, b) => b.relevance - a.relevance);

                    // Filter out links with indistinct relevance
                    links = links.filter( (link, i, arr) => {
                        if ( arr.length == 1 )
                            return link;
                        
                        let prev = arr[i-1];
                        let next = arr[i+1];

                        if ( next && !prev )
                            return link.relevance != next.relevance;
                        if ( prev && next )
                            return link.relevance != prev.relevance && link.relevance != next.relevance;
                        if ( prev && !next )
                            return link.relevance != prev.relevance;
                    });

                    if ( links.length > 0 ) {
                        resolve(links);
                    }
                    else {
                        reject();
                    }
                })
                .catch((error) => {
                    reject(error);
                });;
        });
    }
    search(q) {
        this.queryObj.query = q;
        this.options.path += new URLSearchParams(this.queryObj).toString();

        return new Promise((resolve, reject) => {
            const reqXSearch = https.request(this.options, (res) => {
                let data = '';

                res.on('data', (chunk) => {
                    data += chunk;
                });

                res.on('end', () => {
                    parseString( data, (err, result) => {
                        // At the moment we are only intereseted in exact matches
                        if ( 1 === parseInt(result.xsearch.$.records) ) {
                            resolve(result.xsearch.collection[0].record[0]);
                        }
                        else {
                            reject(result.xsearch.$.records);
                        }
                    });
                });

            });


            reqXSearch.end();

            reqXSearch.on('error', (e) => {
                reject(e);
            });
        });
    }
}

module.exports = {
    LibrisXSearch
};
