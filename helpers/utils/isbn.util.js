class Isbn {
    constructor( isbn ) {
        this.isbn = isbn.replace(/[-\s]/g, '');

        if ( this.isbn.length == 10 ) {
            this.isbn10 = this.isbn;
            this.isbn = this.convert( this.isbn10 );
        }
    }
    get countryOfPublication() {
        // May be used to select national sources
        const countryRanges = {
            '978': [
                { country: 'Sweden', ranges: ['91'] }
            ]
        }

        const prefix = this.isbn.substring(0,3);
        const publisherIdentifier = this.isbn.substring(3, 12);

        if (countryRanges.hasOwnProperty(prefix)) {
            const matchingCountry = countryRanges[prefix].find(
                (countryRange) =>
                    countryRange.ranges.some((range) => publisherIdentifier.startsWith(range))
            );

            if (matchingCountry) {
                return matchingCountry.country;
            }
        }
        
        return null;
    }
    get number() {
        return this.isbn;
    }
    get valid() {
        return this.validate(this.isbn);
    }
    convert( isbn10 ) {
        const isbn = isbn10.replace(/[-\s]/g, '');

        // return if ISBN-13
        if ( isbn.length == 13 )
            return isbn;

        if ( this.validate10(isbn) ) {
            const isbnRoot = isbn.substring(0, isbn.length-1);
            const isbn12 = '978'+isbnRoot;

            return isbn12 + this.getCheckDigit13(isbn12);
        }
    }
    getCheckDigit13( isbn ) {
        const isbn12 = isbn.length == 12 ? isbn : isbn.substring(0, 12);

        let products = [];

        for ( let i = 0; i < isbn12.length; i++ ) {
            if ( 0 == i % 2 ) {
                products.push(isbn12[i]);
            }
            else {
                products.push(isbn12[i] * 3);
            }
        }

        let sum = products.reduce( (previousValue, currentValue) => parseInt(previousValue) + parseInt(currentValue) );
        let remainder = sum % 10;
        let checkDigit = 0 == remainder ? 0 : 10 - remainder;

        return checkDigit;
            
    }
    validate(isbn) {
        if ( isbn.length != 13 ) {
            if ( isbn.length == 10 ) {
                return this.validate10(isbn);
            }
            else {
                return false;
            }
        }

        let checkDigit = this.getCheckDigit13( isbn.substring( 0, 12) );

        if ( isbn.substring(12, 13) == checkDigit ) {
            return true;
        }
        else {
            return false;
        }
    }
    validate10(isbn) {
        // An ISBN-10 number is valid if the sum of the digits multiplied by their position modulo 11 equals zero
        if ( isbn.length != 10 ) {
            return false;
        }

        let products = [];
        
        for ( let i = 0; i < isbn.length; i++ ) {
            let product = isbn[i] * (i+1);
            products.push(product);
        }

        let sum = products.reduce( ( previousValue, currentValue ) => previousValue + currentValue );

        if ( 0 == sum % 11 ) {
            return true;
        }
        else {
            return false;
        }
    }
}

module.exports = {
    Isbn,
}
