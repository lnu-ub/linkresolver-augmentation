/*************************************************/
const https = require('https'),
      crossref = require('./crossref.js'),
      stringsimilarity = require('./helpers/stringsimilarity.js');

const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-oadoi',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });
/*************************************************/
var Oadoi = function () {};

Oadoi.prototype.get_oa_location = function(doi) {
    var options = {
        hostname: 'api.oadoi.org',
        path: '/v2/'+doi+'?email=jakob.nylinnilsson@lnu.se'
    };

    return new Promise((resolve,reject) => {
        var req = https.request(options, (res) => {
            log.debug('query to oadoi');

            if (res.statusCode < 200 || res.statusCode > 299) {
                reject(new Error('Failed to get data from oadoi, status code: ' + res.statusCode));
            }

            var body = '';
            
            res.on('data', function(chunk){
                body += chunk;
            });

            res.on('end', function(){
                try {
                    var response = JSON.parse(body);
                    if ( response.error ) {
                        reject(response.message);
                    }
                    else if ( response.best_oa_location ) {

                        log.debug('got an oa location');

                        // try to get the title from the filename and compare that to the actual title
                        let pdf = response.best_oa_location.url_for_pdf;
                        let basename = pdf.match(/(?<basename>[^\/]+)\.pdf$/i).groups.basename;
                        let wordsInBasename = basename.split('_');
                        let indexOfYear = wordsInBasename.findIndex( (word) => /^\d{4}$/.test( word ) )

                        // Suppose that a year and severals words in the filename indicates the title
                        if ( indexOfYear <= 2 && wordsInBasename.length >= indexOfYear + 3 ) {
                            let levenshtein = stringsimilarity.levenshtein(response.title, wordsInBasename.slice(indexOfYear+1).join(' '));
                            log.debug( 'Levenshtein: ', stringsimilarity.levenshtein(response.title, wordsInBasename.slice(indexOfYear+1).join(' ') ) );
                            if ( levenshtein > 10 ) {
                                log.debug( 'the unpaywall data seems incorrect' );
                                reject('the unpaywall data seems incorrect');
                            }
                        }

                        if ( 'bronze' == response.oa_status && response.best_oa_location.url_for_pdf ) {
                            if ( 'open (via free pdf)' === response.best_oa_location.evidence ) {
                                const evidenceDate = new Date(response.best_oa_location.updated);
                                const now = new Date();

                                // milliseconds
                                if ( ( now - evidenceDate ) > 94670856000 ) {
                                    reject('too old evidence for free pdf');
                                }
                                else {
                                    resolve(response.best_oa_location.url_for_pdf);
                                }
                            }
                            else {
                                resolve(response.best_oa_location.url_for_pdf);
                            }
                        }
                        else if ( 'bronze' != response.oa_status ) {

                            if ( response.best_oa_location.url_for_landing_page ) {
                                resolve(response.best_oa_location.url_for_landing_page);
                            }
                            else {
                                resolve(response.best_oa_location.url_for_pdf);
                            }
                        }
                        else {
                            reject('only bronze-oa available');
                        }
                    }
                    else {
                        reject({msg:'no oa available',data:body});
                    }
                }
                catch (e) {
                    log.debug(e);
                    reject('no oa available');
                }
            });

        });

        req.end();

        req.setTimeout(3000, function() {
            log.debug('timeout when quering oadoi.org');
            req.abort();
        });

        req.on('error', (err) => reject(err));
    });
};

Oadoi.prototype.get_oa_location_by_openurl = function(openurl) {
    var that = this;
    return new Promise((resolve, reject) => {
        crossref.get_doi(openurl)
            .then(function(doi) {
                log.debug('i have a doi :',doi);
                that.get_oa_location(doi)
                    .then(function(url) {
                        resolve(url);
                    }, function(reason) {
                        reject(reason);
                    });
            }, function(reason) {
                reject(reason);
            });
    });
};

module.exports = new Oadoi();
