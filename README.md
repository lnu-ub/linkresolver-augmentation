# Alma Link Resolver Augmentation

This is a node application that do improved link resolving based on ExLibris' Alma. It acts as an interface to the Alma uresolver.

An OpenURL request is sent to this application. There is [one module for requests from Pubmed](pubmed.js) and [another for all other requests](completion.js).

Most of the application logic is in (completion.js)

## How the completion works

The application takes the OpenURL request and calls the backend (e.g. Alma uresolver). The backend tells if we have access to the requested target.

If the backend tells the that application that there should be access the application tries the target with a [HTTP HEAD request](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/HEAD). If the target responds OK the application redirects to the target. If the target does not respons OK the application review the OpenURL request and tries to improve it and correct it if neccessary.

### What the completion can do

- improve the request with metadata from Pubmed or Crossref
- correct misspelled DOI:s
- get the DOI even if it's not in the OpenURL request
- clear confusing metadata
- get a better URL for the target
- find OA
- convert between roman and arabic numerals
- and much more

## Installation

The prefered way tho run the application is as a Node.js process that listens on a port that a normal user can open and then use a reverse proxy that acts as a gateway between the external network and the local application:

- [Reverse Proxy guide for Apache](https://httpd.apache.org/docs/2.4/howto/reverse_proxy.html)
- [Reverse Proxy guide for Nginx](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/)

git is propably the prefered way to deploy to production:
- Create a folder to deploy to on production server
- Add a bare repository on the productions server
- Add the post-receive hook script to the bare repository (and make it executable)
- Add the remote-repository resided on the production server to your local repository


## Questions
Plese send [jakob.nylinnilsson@lnu.se](mailto:jakob.nylinnilsson@lnu.se.com?subject=[GitLab]%20linkresolver-augmentation) an email if you have any questions

