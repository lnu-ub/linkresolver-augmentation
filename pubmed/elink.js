/* jshint esversion: 6 */
const Eutils = require('./eutils.js');
const https = require('https');

var Elink = function(id, par) {
    // llinks gives all links
    // prlinks gives the publisher link
    this._eutils = new Eutils('elink', {id: id, cmd: 'llinks', retmode: 'JSON'});
};

Elink.prototype.freeUrl = function() {
    return new Promise((resolve, reject) => {
        var req = https.request(this._eutils.options, (res) => {
            let data = '';
            
            res.on('data', (chunk) => {
                data += chunk;
            });
            
            res.on('end', () => {
                var parsedData = JSON.parse(data);

                if (!parsedData.linksets || parsedData.linksets.length < 1 ) {
                    reject('no links in pubmed');
                }
                else {
                    let idurllist = parsedData.linksets[0].idurllist[0];

                    if ( idurllist.objurls.length > 0 ) {
                        idurllist.objurls.forEach(function(objurl) {
                            if ( objurl.attributes.some( a => 'free resource' == a ) &&
                                 objurl.categories.some( c => c.toLowerCase().startsWith('full text') )
                               ) {
                                resolve(objurl.url.value.replace(/&amp;/g,"&"));
                            }
                                 if ( objurl.attributes.indexOf("free resource") > -1 &&
                                 objurl.attributes.find(function(element) {
                                     return element.startsWith("full-text") ? true : false;
                                 })
                               ) {
                                resolve(objurl.url.value.replace(/&amp;/g,"&"));
                            }
                        });
                    }
                
                    reject('no free fulltext in pubmed');
                }


            });
            
        }).on('error', (err) => {
            console.log("Error: " + err.message);
        });

        req.end();
    });

};

Elink.prototype.prUrl = function() {
    return new Promise((resolve, reject) => {
        var req = https.request(this._eutils.options, (res) => {
            let data = '';
            
            res.on('data', (chunk) => {
                data += chunk;
            });
            
            res.on('end', () => {
                try {
                    var parsedData = JSON.parse(data);

                    if ( parsedData.linksets.length < 1 ) {
                        reject('no links in pubmed');
                    }
                    else {
                        let idurllist = parsedData.linksets[0].idurllist[0];

                        if ( idurllist.objurls.length > 0 ) {
                            idurllist.objurls.forEach(function(objurl) {
                                if ( objurl.attributes.indexOf("publisher of information in url") > -1 &&
                                     objurl.attributes.find(function(element) {
                                         return element.startsWith("full-text") ? true : false;
                                     })
                                   ) {
                                    objurl.url.value = objurl.url.value.replace(/&amp;/g,"&");
                                    resolve(objurl);
                                }
                            });
                        }
                
                        reject('no publisher fulltext in pubmed');
                    }
                }
                catch (e) {
                    reject(e);
                }
            });
            
        }).on('error', (err) => {
            console.log("Error: " + err.message);
        });

        req.end();
    });

};

module.exports = Elink;
                        
