/* jshint esversion: 6 */
const PmcUtils = require('./pmc.js');
const https = require('https');

var IdConv = function(ids, par) {
    // ids can be one or more and must of the same type: PMCID, PMID or DOI
    // possible parameters: ids, idtype, format
    this._pmcutils = new PmcUtils('idconv/v1.0', {ids: ids, format: 'json'});
};

IdConv.prototype.getResults = function() {
    return new Promise((resolve, reject) => {
        var req = https.request(this._pmcutils.options, (res) => {
            let data = '';

            res.on('data', (chunk) => {
                data += chunk;
            });

            res.on('end', () => {
                var parsedData = JSON.parse(data);

                if (!parsedData.records[0].status) {
                    resolve(parsedData);
                }
                else {
                    reject('not in pubmed');
                }
            });
        }).on('error', (err) => {
            console.log("Error: " + err.message);
        });;

        req.end();
    });
};

module.exports = IdConv;
