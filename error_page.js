/*************************************************/
const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-errorpage',
          streams: [
              {
                  level: 'info',
                  stream: process.stdout
              },
              {
                  level: 'error',
                  stream: process.stderr
              }
          ]
      });

const querystring = require('querystring');
const doT = require('dot');
const nodemailer = require('nodemailer');
/*************************************************/
var ErrorPage = function() {};

ErrorPage.prototype.build_html = function(context, res, cto, OPEN_URL_REQUEST) {
    var my_cto = this._parse_cto(cto),
        my_report = my_cto;

    my_report.context = context;
    my_report.OPEN_URL_REQUEST = OPEN_URL_REQUEST;

    // Skicka rapport till mantis
    this._report(my_report);

    var dots = require("dot").process({ path: "./views"});
    var html = dots.errorpage(my_report);

    res.end(html, 'utf-8');
}

ErrorPage.prototype._parse_cto = function(cto) {
    var cto_helper = require('./helpers/cto.js');

    let my_cto = cto_helper.my_cto(cto);

    return cto_helper.my_cto(cto);
};

ErrorPage.prototype._report = function(cto) {
    var dots = require("dot").process({ path: "./views/mail"});

    const transporter = nodemailer.createTransport({
        host: 'localhost',
        port: 25,
    });

    var mailOptions = {
        from: '"Jared Atwater" joenab@lnu.se',
        to: 'ub-infra@lnu.se',
        subject: 'Trasig länk i länkservern',
        text: dots.textreport(cto),
        html: dots.htmlreport(cto)
    };

    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return log.error(error);
        }
        log.debug('Message sent: %s', info.messageId);
        log.debug('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    });
};

module.exports = new ErrorPage();

