/* jshint esversion: 8 */
/*************************************************/
const http = require('http'),
      https = require('https'),
      xml2js = require('xml2js'),
      concat = require('concat-stream'),
      parser = new xml2js.Parser(),
      querystring = require('querystring');

const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-crossref',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });
/*************************************************/
var Crossref = function () {
    this.api_options = {
        hostname: 'api.crossref.org',
        headers: {
            'User-Agent': 'Alma Link Resolver Augmentation/'+process.env.npm_package_version+' (mailto:'+this._get_config(context).admin.email+')'
        }
    };

    this.questionInProgress = false;
};

Crossref.prototype.get_title_match = function(issn, title) {
    let that = this;
    let options = Object.assign({},this.api_options);

    log.debug('Get title match from Crossref');

    return new Promise((resolve, reject) => {
        let candidates = [];
        
        options.path = `/journals/${encodeURIComponent(issn)}/works?query.title=${encodeURIComponent(title)}&select=DOI,title,volume,published&rows=2`;

        log.debug('restful query to crossref (title match): ' + options.path);
            
        var req = https.request(options, (res) => {
            if (res.statusCode < 200 || res.statusCode > 299) {
                reject(new Error('Failed to get data from crossref rest api, status code: ' + res.statusCode));
            }
                
            var str = '',
                json;

            res.on('data', function(chunk) {
                str += chunk;
            });
            
            res.on('end', function() {
                try {
                    json = JSON.parse(str);
                    
                    if ( Number(json.message['total-results']) === 1 ) {
                        resolve(json.message.items[0].DOI);
                    }
                    else if ( Number(json.message['total-results']) > 1 ) {
                        for (let i = 0; i < Number(json.message['total-results']); i++ ) {
                            let item = json.message.items[i];
                                
                            let my_title = title.replace(/["”–-]/g,"").toLowerCase();
                            let item_title = item.title[0].replace(/["”–-]/g,"").toLowerCase();

                            if (my_title.includes(item_title) || ( item_title.indexOf(my_title) === 0 ) ) {
                                candidates.push(item);
                            }

                            if ( i == i < Number(json.message['total-results']) ) {
                                resolve(candidates);                                
                            }
                        }
                    }
                    else {
                        reject('No DOI candidates');
                    }
                }
                catch (e) {
                    reject(e);
                }
            });
        });

        req.end();

        // The API can be slow sometimes but it's worth waiting for
        req.setTimeout(12000, function() {
            log.debug('crossref api responded slow');
            req.abort();
        });
	    
        req.on('error', (err) => reject(err));
    });
};

Crossref.prototype.get_resource_url_candidates = function(openurl) {
    let that = this;

    log.debug('Get resource URL candidates');
    
    return new Promise((resolve, reject) => {
	let q = querystring.parse(openurl);
        let resource_url_candidates = [];

        let get_candidates = async function() {
            try {
                // Candidates taken from title
	        let doi_candidates = await that._doi_candidates_from_api(q.issn || q["rft.issn"],q.au || q["rft.au"] || q.aulast || q["rft.aulast"],q.atitle || q["rft.atitle"],q.volume || q["rft.volume"],q.year || q["rft.year"], q.spage || q["rft.spage"]);

                if (doi_candidates) {
                    if ( doi_candidates.length > 0) {
                        log.debug('This resource has ' + doi_candidates.length + ' DOI candidates');
                    
                        let i;
                        for (i = 0; i < doi_candidates.length; i++) {
                            let url = await that.get_resource_url_from_doi(doi_candidates[i]);

                            if (url) {
                                resource_url_candidates.push(url);
                            }
                        }
                    }
                    else {
                        log.debug('No DOI candites from API, try get single resource url');

                        // This can also use page numbering to get the resource url
                        let resource_url = await that.get_resource_url(openurl);
                        if (resource_url) {
                            resource_url_candidates.push(resource_url);
                        }
                    }

                    this.questionInProgress = false;

                    if (resource_url_candidates.length > 0) {
                        resolve(resource_url_candidates);
                    }
                    else {
                        reject('No URL candidates for this OpenURL');
                    }
                }
            }
            catch (e) {
                reject(e);
            }
        };
	
        get_candidates();
    });
};

Crossref.prototype.get_resource_url = function(openurl) {
    return new Promise((resolve,reject) => {
        this._openurl_query(openurl)
            .then(function(url) {
                resolve(url);
            }, function(reason) {
                var q = querystring.parse(openurl);

                if ( !q.pages && q.spage ) {
                    q.pages = q.spage+'-'+q.spage;
                }

                if ( q.issn && ( q.aulast || q.au ) && q.atitle ) {
                    let author = q.aulast || q.au;
                    module.exports._doi_from_api_by_title(q.issn,author,q.atitle)
                        .then(function(doi) {
                            module.exports._openurl_query("id=doi:"+doi,"resource","doi")
                                .then(function(url) {
                                    resolve(url);
                                }, function(reason) {
                                    reject(reason);
                                });
                        }, function(reason) {
                            if ( q.year && q.volume && q.pages ) {
                                module.exports._doi_from_api_by_pages(q.issn, q.volume, q.year, q.issue, q.pages)
                                    .then(function(doi) {
                                        module.exports._openurl_query("id=doi:"+doi,"resource","doi")
                                            .then(function(url) {
                                                resolve(url);
                                            }, function(reason) {
                                                reject(reason);
                                            });
                                    }, function(reason) {
                                        reject(reason);
                                    });
                            }
                            else {
                                reject(reason);
                            }
                        });
                }
                else if ( q.year && q.volume && q.pages ) {
                    module.exports._doi_from_api_by_pages(q.issn, q.volume, q.year, q.issue, q.pages)
                        .then(function(doi) {
                            module.exports._openurl_query("id=doi:"+doi,"resource","doi")
                                .then(function(url) {
                                    resolve(url);
                                }, function(reason) {
                                    reject(reason);
                                });
                        }, function(reason) {
                            reject(reason);
                        });
                }
                else {
                    reject(reason);
                }
            });
    });
};

Crossref.prototype.get_resource_url_from_doi = function(doi) {
    return new Promise((resolve,reject) => {
        module.exports._openurl_query("id=doi:"+doi,"resource","doi")
            .then(function(url) {
                if ( 'string' === typeof url ) {
                    resolve(url);
                }
                else {
                    resolve(url._);
                }
            }, function(reason) {
                reject(reason);
            });
    });
};

Crossref.prototype.filter_license = function(doi) {
    let options = Object.assign({
        path: '/works/?filter=doi:'+doi+',has-license:true,license.version:vor'
    }, this.api_options);

    return new Promise((resolve,reject) => {
        var req = https.request(options, function(res) {
            log.debug('checking license information for ' + doi);
            console.log('statusCode', res.statusCode);
            
            var str = '',
                json;

            res.on('data', function(chunk) {
                str += chunk;
            });

            res.on('end', function() {
                try {
                    json = JSON.parse(str);
                    resolve(json);
                }
                catch (e) {
                    reject(e);
                }
            });
        });

        req.end();

        req.setTimeout(5000, function() {
            log.debug('crossref api responded slow');
            req.abort();
        });

        req.on('error', (err) => reject(err));

    });
};

Crossref.prototype.get_doi = function(openurl) {
    return new Promise((resolve,reject) => {
        this._openurl_query(openurl,"doi_data")
            .then(function(doi_data) {
                resolve(doi_data.doi[0]);
            }, function(reason) {
                reject(reason);
            });
    });
};

Crossref.prototype.get_journal_article = function(openurl) {
    return new Promise((resolve,reject) => {
        this._openurl_query(openurl,"journal_article")
            .then(function(journal_article) {
                resolve(journal_article);
            }, function(reason) {
                reject(reason);
            });
    });
};

Crossref.prototype.parse_journal_article = function(journal_article) {
    let obj = {};
    
    let first_publication_year = 0;
    journal_article.publication_date.forEach(publication_date => {
        if ( first_publication_year == 0 ) {
            first_publication_year = publication_date.year[0];
        }
        else if ( publication_date.year[0] < first_pulibcation_year ) {
            first_publication_year = publication_date.year[0];
        }
    });

    let doi = '';
    if ( journal_article.doi_data.length == 1 ) {
        obj.doi = journal_article.doi_data[0].doi[0];
    }


    // $
    // titles
    // contributors
    // publication_date
    // pages
    // publisher_item
    // program
    // doi_data
    // citation_list

    try {
        obj.year = first_publication_year;
        obj.title = journal_article.titles[0].title[0];
        obj.end_page = journal_article.pages[0].last_page[0];
    }
    catch (e) {
        console.error(e);
    }

    return obj;
}

Crossref.prototype.get_metadata = function(openurl) {
    return new Promise((resolve,reject) => {
        this._openurl_query(openurl,"doi_data")
            .then(function(doi_data) {
                resolve(doi_data);
            }, function(reason) {
                reject(reason);
            });
    });
};

Crossref.prototype.get_crossref_data_from_doi = function(doi) {
    let idValue = `doi:${doi}`;
        
    return new Promise((resolve,reject) => {
        this._id_query(idValue)
            .then(function(doi_data) {
                resolve(doi_data);
            }, function(reason) {
                reject(reason);
            });
    });
};


Crossref.prototype.doi_status = function(doi) {
    let options = Object.assign({
        path: '/works/'+doi,
        method: 'HEAD'
    }, this.api_options);

    log.debug('checking doi status');

    return new Promise((resolve,reject) => {
        var req = https.request(options, function(res) {
            resolve(Number(res.statusCode));
        });

        req.on('error', function(err) {
            log.debug(err);
        });
        
        req.end();
    });
};

// https://api.crossref.org/journals/0140-2382/works?query.bibliographic=Vol.%2025(2002):3&mailto=jakob.nylinnilsson@lnu.se&select=issue,DOI,published-print,published-online,title,volume,page
// issn, volume, year, issue
// https://github.com/CrossRef/rest-api-doc
Crossref.prototype._doi_from_api_by_title = function(issn, author, title) {
    return this._doi_from_api(issn, author, title);
};

Crossref.prototype._doi_from_api_by_pages = function(issn, volume, year, issue, pages) {
    return this._doi_from_api(issn, null, null, volume, year, issue, pages);
};

Crossref.prototype._doi_candidates_from_api = function(issn, author, title, volume, year, issue, pages) {
    let options = Object.assign({},this.api_options);
    
    return new Promise((resolve, reject) => {
        let candidates = [];

        if (issn) {
            options.path = '/journals/' + encodeURIComponent(issn) + '/works?';

            if ( author && title ) {
                options.path += 'query.author=' + encodeURIComponent(author);
                options.path += '&query.title=' + encodeURIComponent(title);
                options.path += '&query.bibliographic=' + encodeURIComponent(year);
                options.path += '&select=DOI,title,author';
            }
            else if ( volume && year && issue && pages ) {
                options.path += 'query.bibliographic=' + encodeURIComponent('Vol. '+volume+'('+year+'):'+issue);

                options.path += '&select=volume,issue,page,DOI&rows=100'; //60
            }
            else if ( title ) {
                options.path += 'query.bibliographic=' + encodeURIComponent(title) + encodeURIComponent(' ') + encodeURIComponent(year);
                options.path += '&select=DOI,title,author,volume,page&rows=2';
            }
        }

        if ( options.path.indexOf('query') > -1 ) {
            log.debug('restful query to crossref (candidates): ' + options.path);
            
            var req = https.request(options, (res) => {
                if (res.statusCode < 200 || res.statusCode > 299) {
                    reject(new Error('Failed to get data from crossref rest api, status code: ' + res.statusCode));
                }
                
                var str = '',
                    json;

                res.on('data', function(chunk) {
                    str += chunk;
                });

                res.on('end', function() {
                    try {
                        json = JSON.parse(str);

                        if ( Number(json.message['total-results']) === 1 ) {
                            resolve(json.message.items[0].DOI);
                        }
                        else if ( Number(json.message['total-results']) > 1 ) {
                            for (let i = 0; i < Number(json.message['total-results']); i++ ) {
                                let item = json.message.items[i];
                                
                                let my_title = title.replace(/["”–-]/g,"").toLowerCase();
                                let item_title = item.title[0].replace(/["”–-]/g,"").toLowerCase();

                                if (my_title.includes(item_title) || ( item_title.indexOf(my_title) === 0 ) ) {
                                    let candidate = item.DOI;
                                    candidates.push(candidate);
                                }

                                if ( i == i < Number(json.message['total-results']) ) {
                                    resolve(candidates);                                
                                }
                            }
                        }
                        else {
                            reject('No DOI candidates');
                        }
                    }
                    catch (e) {
                        reject(e);
                    }
                });
            });

            req.end();

            // The API can be slow sometimes but it's worth waiting for
            req.setTimeout(6000, function() {
                log.debug('crossref api responded slow');
                req.abort();
            });
	    
            req.on('error', (err) => reject(err));
        }
        else {
            reject('No query to Crossref');
        }
    });
};



Crossref.prototype._doi_from_api = function(issn, author, title, volume, year, issue, pages) {
    let options = Object.assign({},this.api_options);

    return new Promise((resolve, reject) => {
        if ( issn ) {
            options.path = '/journals/' + encodeURIComponent(issn) + '/works?';

            if ( author && title ) {
                options.path += 'query.author=' + encodeURIComponent(author) + '&query.bibliographic=' + encodeURIComponent(title);
                options.path += '&select=DOI,title,author';
            }
            else if ( volume && year && issue && pages ) {
                options.path += 'query.bibliographic=' + encodeURIComponent('Vol. '+volume+'('+year+'):'+issue);
                options.path += '&select=volume,issue,page,DOI&rows=100'; //60
            }
        }

        if ( options.path.indexOf('query') > -1 ) {
            log.debug('restful query to crossref: ' + options.path);
            
            var req = https.request(options, (res) => {
                if (res.statusCode < 200 || res.statusCode > 299) {
                    reject(new Error('Failed to get data from crossref rest api, status code: ' + res.statusCode));
                }
                
                var str = '',
                    json;

                res.on('data', function(chunk) {
                    str += chunk;
                });

                res.on('end', function() {
                    try {
                        json = JSON.parse(str);
                        
                        if ( Number(json.message['total-results']) === 1 ) {
                            resolve(json.message.items[0].DOI);
                        }
                        else {
                            var needle;

                            if ( title ) {
                                needle = json.message.items.find(function(item){
                                    var needle = item.author.find(function(a) {
                                        return a.family.toLowerCase() === author.toLowerCase();
                                    });

                                    if ( needle ) {
                                        return item.title[0].toLowerCase() === title.toLowerCase();
                                    }
                                    else {
                                        return false;
                                    }
                                });
                            }
                            else if ( pages ) {
                                var patt = {
                                    issue: new RegExp("\["+issue+"\]\[-\/\]\[\\d*\]\|\[\\d*\]\[-\/\]\["+issue+"\]\|\D*"+issue),
                                    page: new RegExp("\(\(\\D\-\?\)\?\\d*\)\-\(\(\\D\-\?\)\?\\d*\)"),
                                    pagPart: new RegExp("\(\[A\-Z\]*\)\-\?\(\\d*\)"),
                                };


                                var spage = pages.replace(patt.page,"$1"),
                                    epage = pages.replace(patt.page,"$3");

                                needle = json.message.items.find(function(item) {
                                    if ( item.page ) {
                                        item.spage = item.page.replace(patt.page,"$1");
                                        item.epage = item.page.replace(patt.page,"$3");

                                        // pages ∈ item.pages
                                        if ( volume == item.volume &&
                                             ( issue == item.issue || patt.issue.test(item.issue) ) &&
                                             Number(spage) >= Number(item.spage) && Number(epage) <= Number(item.epage) ) {
                                            return true;
                                        }
                                        else if ( isNaN(spage) && isNaN(item.spage) ) {
                                            // abstract and supplements and unusual pagination
                                            if ( patt.issue.test(item.issue) && spage.replace(patt.pagPart,"$1") == item.spage.replace(patt.pagPart,"$1") ) {
                                                if ( spage.replace(patt.pagPart,"$2") >= item.spage.replace(patt.pagPart,"$2") && Number(epage.replace(patt.pagPart,"$2")) <= Number(item.epage.replace(patt.pagPart,"$2"))
                                                   ) {
                                                    return true;
                                                }
                                            }
                                        }

                                    }

                                    
                                    return false;
                                });
                            }

                            if ( needle ) {
                                resolve(needle.DOI);
                            }
                            else {
                                reject('total results: '+json.message['total-results']);
                            }
                        }
                        
                    } catch(err) {
                        reject(err);
                    }
                });
            });

            req.end();

            req.setTimeout(3000, function() {
                log.debug('crossref api responded slow');
                req.abort();
            });
    
            req.on('error', (err) => reject(err));
        }
        else {
            reject('required arguments is missing');
        }
    });

};

Crossref.prototype._get_config = function(context, cache = true) {
    var confFileName = './config/'+context+'.js';
    if ( cache ) {
        delete require.cache[require.resolve(confFileName)];
    }
    return require(confFileName);
};

Crossref.prototype._id_query = function(idValue) {
    var conf = this._get_config(context).crossref;
    
    var options = {
        hostname: 'doi.crossref.org',
        path: '/openurl?pid='+conf.username+':'+conf.password+'&format=unixref&'
    };
    options.path += `id=${idValue}`;

    return new Promise((resolve,reject) => {
        var req = https.request(options, (res) => {
            if (res.statusCode < 200 || res.statusCode > 299) {
                reject(new Error('Failed to get data from crossref, status code: ' + res.statusCode));
            }

            res.pipe(concat(function(buffer) {
                var str = buffer.toString();

                parser.parseString(str, function(err, result) {
                    try {
                        if ( result.doi_records.doi_record[0].crossref[0].error ) {

                            reject(result.doi_records.doi_record[0].crossref[0].error[0]);
                        }
                        else {
                            resolve(result.doi_records.doi_record[0].crossref[0]);
                        }
                    }
                    catch (e) {
                        console.error(e);
                    }
                });
            }));
        });

        req.end();

        req.on('error', (err) => reject(err));

    });
        
};
    

Crossref.prototype._openurl_query = function(openurl, select = "resource", query_type = "metadata" ) {
    var conf = this._get_config(context).crossref;
    
    var options = {
        hostname: 'doi.crossref.org',
        path: '/openurl?pid='+conf.username+':'+conf.password+'&format=unixref&'
    };
    
    var args_to_crossref = this._clean_openurl(openurl, query_type);

    return new Promise((resolve,reject) => {
        if ( args_to_crossref ) {
            options.path += args_to_crossref;

            var req = http.request(options, (res) => {
                log.debug('openurl query ('+query_type+') to crossref');
            
                if (res.statusCode < 200 || res.statusCode > 299) {
                    reject(new Error('Failed to get data from crossref, status code: ' + res.statusCode));
                }

                this.questionInProgress = false;

                res.pipe(concat(function(buffer) {
                    var str = buffer.toString();

                    parser.parseString(str, function(err, result) {
                        try {
                            
                            if ( result.doi_records.doi_record[0].crossref[0].error ) {
                                reject(result.doi_records.doi_record[0].crossref[0].error[0]);
                            }
                            else {
                                // Kan vi lita på att DOI-posterna ser ut så här?
                                // proceedings ser t.ex. annorlunda ut

                                let crossref_data = result.doi_records.doi_record[0].crossref[0];
                                switch ( select.toLowerCase() ) {
                                case 'resource':
                                    var rec = result.doi_records.doi_record[0].crossref[0],
                                    resource_url,
                                    doi_data,
                                    doi;
                                    
                                    if ( rec ) {
                                        if  ( rec.journal ) {
                                            doi_data = rec.journal[0].journal_article[0].doi_data[0];
                                        }
                                        else if ( rec.book ) {
                                            doi_data = rec.book[0].content_item[0].doi_data[0];
                                        }
                                        resource_url = doi_data.resource[0];
                                        doi = doi_data.doi[0];
                                    }

                                    if ( resource_url ) {
                                        log.debug('returning resource_url from crossref ('+doi+'): '+resource_url);
                                        resolve(resource_url);
                                    }
                                    else {
                                        reject('no url from crossref');
                                    }

                                
                                    break;
                                case 'doi_data':
                                    log.debug('found doi record');
                        
                                    // Metadatan för proceedings har annan struktur än metadatan för tidskrifter

                                    let this_doi_data;
                                    if ( crossref_data.journal ) {
                                        this_doi_data = crossref_data.journal[0].journal_article[0].doi_data[0];
                                    }
                                    else if ( crossref_data.book ) {
                                        this_doi_data = crossref_data.book[0].content_item[0].doi_data[0];
                                    }
                                    else {
                                        reject("no doi data we can use");
                                        log.debug("we can't use the doi data");
                                        console.log( crossref_data );
                                    }

                                    resolve(this_doi_data);

                                    break;
                                case 'journal_article':
                                    log.debug('found journal article');


                                    let journal_article;

                                    if ( crossref_data.journal ) {
                                        journal_article = crossref_data.journal[0].journal_article[0];
                                    }
                                    else {
                                        reject("no doi data we can use");
                                        log.debug("we can't use the doi data");
                                        log.debug(crossref_data);
                                    }

                                    resolve(journal_article);

                                    break;
                                default:
                                    reject('unsupported select value');
                                
                                }

                            }



            
                        }
                        catch(e) {
                            reject(e);
                        }
                    });
                }));

                //res.on('error', (err) => reject(err));

            });

            req.end();
            
            req.on('error', (err) => reject(err));


        }
        else {
            reject("missing arguments to query crossref");
        }
    });
};

//-------------
Crossref.prototype._clean_openurl = function(openurl, query_type) {
    var q = querystring.parse( openurl.replace(/rft\./g,"") ),
        crossref_q = {};

    if ( query_type === "doi" ) {
        crossref_q.id = q.id;
        
        return querystring.stringify(crossref_q);
    }
    else {
        // querystring parses undefined values to the string "undefined"
        if ( q.eissn !== "undefined" || q.issn !== "undefined" ) {
            crossref_q.eissn = q.eissn;
            crossref_q.issn = q.issn;
            
            crossref_q.date = q.date ? q.date.replace(/^(\d{4}).*/,"$1") : q.year;

            crossref_q.volume = q.volume;
        
            // Either first page or author must be supplied
            if ( q.spage ) {
                crossref_q.spage = q.spage.replace(/(\d*)\-\d*/i, "$1");
            }
            crossref_q.aulast = q.aulast;


            // Issue and atitle is often wrongly in the openurl data
            if ( q.issue ) {
                if ( q.issue.match(/^suppl/i) ) {
                    crossref_q.atitle = q.atitle;
                    crossref_q.aulast = q.aulast;
                }
                else {
                    crossref_q.issue = q.issue;
                }
            }
            else {
                crossref_q.atitle = q.atitle;
            }

            return querystring.stringify(crossref_q);
        }
        else {
            return;
        }
    }
};

module.exports = new Crossref();
