/*************************************************/
const querystring = require('querystring');

const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-ovid',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });

const path =  require('path');
var defaultConfFileName = path.resolve(__dirname, '../config/default.js');
const conf = require(defaultConfFileName).completion.ovid;
/*************************************************/
module.exports = function(openurl, doi) {
    var module = {};

    module.get_target_url = function(target_url) {
        var uresolver_target = {
            proxified_url: "string" === typeof target_url ? target_url : target_url[0]
        };

        var match = uresolver_target.proxified_url.match(/(https?:\/\/ovid.*\?)(.*$)/i);

        uresolver_target.url = match[0];
        uresolver_target.querystring = match[2];

        const ovid_oce_base = 'https://oce.ovid.com/search?';
        var ovid_base_url = match[1],
            q = querystring.parse( uresolver_target.querystring );

        // Vi bygger en egen sökning om ExLibris inte har fulltext-länk
        // men även om det finns doi, eftersom uresolver inte använder doi för Ovid
        if ( 'toc' === q.PAGE || 'titles' === q.PAGE ) {
            log.debug('building ovid search');
            var ovid_search = _get_ovid_search(openurl, doi);
            var ovid_oce_search = {
                q: _get_ovid_search(openurl, doi, true)
            }
            
            if ( ovid_search ) {
                delete q.AN;
                q.CSC = 'Y';
                q.checkipval ='yes';
                q.SEARCH = ovid_search;
                q.NEWS = 'N';
                q.D = 'ovft';
                //q.T = 'JS';
                q.PAGE = 'fulltext';
                q.MODE = 'ovid';

                if ( conf.oce ) {
                    return ovid_oce_base + querystring.stringify(ovid_oce_search);
                }
                else {
                    return ovid_base_url + querystring.stringify(q);
                }
            }
            else {
                return uresolver_target.url;
            }
        }
	else if ( 'fulltext' === q.PAGE ) {
            // Exlibris bygger förhoppningsvis fulltextlänkar så här: "0028-3878".is and "87".vo and "3".ip and "299".pg
            log.debug('uresolver has fulltext link');

	    return uresolver_target.url;
	}
	else {
	    return;
	}

    };

    function _get_ovid_search(openurl, doi, oce = false) {
        const openurl_request = querystring.parse( openurl );
        const illegal_char = /[\#\"\&\%\:\;\.\/\(\)]/ig;
        const stop_words = /(^|\s)([^\s]*[α-ω][^\s]*?)(\s|$)/ig;
        
        let doiQueryPart, metaQueryPart;
        
        if ( doi ) {
            log.debug('ovid search based on doi:'+doi);
            let an = doi.replace(/^10.\d{4,9}\/(\d{8}-\d{9}-\d{5})/i,"$1");

            let query = oce ?
                'DOI:('+doi+')' :
                '"'+doi+'".di';

            if (24 == an.length) {
                query += oce ?
                    ' or AccessionNumber:('+an+')' :
                    ' or "'+an+'".an';
            }

            doiQueryPart = query;
        }

        let issn, volume, issue, spage, atitle, aulast;
        issn = openurl_request['rft.issn'] || openurl_request.issn || '';
        volume = openurl_request['rft.volume'] || openurl_request.volume || '';
        atitle = openurl_request['rft.atitle'] || openurl_request.atitle || '';
        issue = openurl_request['rft.issue'] || openurl_request.issue || '';
        spage = openurl_request['rft.spage'] || openurl_request.spage || '';
        aulast = openurl_request['rft.aulast'] || openurl_request.aulast || '';

        if ( issn ) {
            if ( atitle ) {
                atitle = atitle.replace(illegal_char,'');
                atitle = atitle.replace(stop_words,'');
            }

            if ( volume && issue && spage ) {
                metaQueryPart = oce ?
                    'ISSN:('+issn+') and Volume:('+volume+') and Issue:('+issue+') and FirstPage:('+spage+')' :
                    '('+issn+').is and ('+volume+').vo and ('+issue+').is and ('+spage+').pg ';
            }
            else if ( atitle && aulast ) {
                metaQueryPart = oce ?
                    'Issue:('+issn+') and ( Title:('+atitle+') or Author:('+aulast+') )' :
                    '('+issn+').is and ( ('+atitle+').ti or ('+aulast+').au )';
            }
        }


        if ( doiQueryPart && metaQueryPart ) {
            return `(${doiQueryPart}) or (${metaQueryPart})`;
        }
        else if ( doiQueryPart ) {
            return `${doiQueryPart}`;
        }
        else if ( metaQueryPart ) {
            return `${metaQueryPart}`;
        }
        else {
        }

    }

    return module;
};
