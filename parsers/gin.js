/* jshint esversion: 8 */
/*************************************************/
const querystring = require('querystring');
/*************************************************/

var GetItNow = function() {
};

GetItNow.prototype.get_target_url = function(parsed_cto,gin_service) {
    return new Promise((resolve, reject) => {
        try {
            let cto = parsed_cto,
                service = gin_service;

            let parse_parameters = service.key.parse_parameters;
            
            let ccc_url = parse_parameters.match(/ccc_url=([^&]*)/)[1].trim();
            let qs  = {
                illEmail: parse_parameters.match(/email=([^&]*)/)[1],
                orderSource: parse_parameters.match(/source=([^&]*)/)[1],
                userBilled: parse_parameters.match(/bill=([^&]*)/)[1],
                institute: parse_parameters.match(/institute=([^&]*)/)[1],
                partner: 'ExLibris',
                directURL: 'https://getitnow.copyright.com/requestortho'
            };

            qs.title = cto['rft.atitle'];
            qs.publisherName = qs.copyright = cto['rft.pub'];
            qs.contentID = qs.publication = cto['rft.issn'];
            qs.publicationDate = cto['rft.date'];
            qs.libraryUserID = '';
            qs.author = cto['rft.aulast'] && cto['rft.aufirst'] ? cto['rft.aulast'] + ', ' + cto['rft.aufirst'] : '';
            qs.startPage = cto['rft.spage'] ? cto['rft.spage'] : '' ;
            qs.endPage = cto['rft.epage'] ? cto['rft.epage'] : '' ;
            qs.volumeNum = cto['rft.volume'] ? cto['rft.volume'] : '';
            qs.issueNum = cto['rft.issue'] ? cto['rft.issue'] : '';
            
            let target_url = ccc_url + '?' + querystring.stringify(qs);

            resolve(target_url);
        }
        catch (e) {
            reject(e);
        }
    });
};

module.exports = new GetItNow();
