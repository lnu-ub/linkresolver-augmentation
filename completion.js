/* jshint esversion: 8 */
/*************************************************/
const https = require('https'),
      xml2js = require('xml2js'),
      concat = require('concat-stream'),
      stripPrefix = require('xml2js').processors.stripPrefix,
      parser = new xml2js.Parser({
          tagNameProcessors: [ stripPrefix ],
          attrNameProcessors: [ stripPrefix ],
      }),
      querystring = require('querystring'),
      path =  require('path'),
      cto_helper = require('./helpers/cto.js');


const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation-completion',
          streams: [
              {
                  level: 'debug',
                  stream: process.stdout
              },
          ]
      });

/*** Redis ***/
const redis = require("redis");
const clients = {};
(function () {
    try {
        var defaultConfFileName = path.resolve(__dirname, './config/default.js');
        console.log(defaultConfFileName);
        const conf = require(defaultConfFileName).completion;

        const redis_options = {
            legacy_mode: true,
            retry_strategy: function(options) {
                if (options.error && options.error.code === "ECONNREFUSED") {
                    return new Error("The server refused the connection");
                }
                if (options.total_retry_time > 1000 * 60 * 60) {
                    return new Error("Retry time exhausted");
                }
                if (options.attempt > 10) {
                    return undefined;
                }
                return Math.min(options.attempt * 100, 3000);
            },
        };

        if ( conf.redis && conf.redis.unixsocket ) {
            redis_options.path = conf.redis.unixsocket;
        }

        const cacheInstance = redis.createClient(redis_options);
        cacheInstance.connect();
        clients.cacheInstance = cacheInstance;
    }
    catch (e) {
        console.log(e);
    }
})();

clients.cacheInstance.on("error", function(error) {
    console.error(error);
});


/*************************************************/
module.exports = function(req, res, context) {
    var module = {};

    //---- Config
    global.context = context;
    
    log.debug('we are in '+context+' context');

    var confFileName = './config/'+context+'.js';
    
    log.debug('reading config from '+confFileName);

    /* As the default config is used in different contexts the cache
       has to be cleared */
    if ( "default" === context ) {
        delete require.cache[require.resolve(confFileName)];
    }
    var conf = require(confFileName).completion;
    const uresolver = require('./helpers/uresolver.js'),
          libkey = require('./helpers/libkey.js'),
          crossref = require('./crossref.js'),
          oadoi = require('./oadoi.js'),
          GetItNow = require('./parsers/gin.js');

    /* Redis */
    var client;
    try {
        client = clients.cacheInstance;
    }
    catch (e) {
        console.error(e);
    }
    //----

    const reProxy = new RegExp(conf.url.proxyPrefix.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'),"gi");
    var services,
        our_services;

    module.resolve = function(openurl, self = false) {

        if ( openurl ) {
            // save the origin openurl for later use
            this._OPEN_URL_REQUEST = openurl;
            log.debug('called with this openurl:');
            log.debug(this._OPEN_URL_REQUEST);
            
            // clean the openurl from some Alma/Primo settings or bad metadata
            // and get the doi
            openurl = _clean_openurl(openurl);
            var doi, pmid;
            try {
                doi = _get_doi(openurl);
                pmid = _get_pmid(openurl);
            }
            catch (e) {
                console.error(e);
            }
                

            // Don't trust Primo if we have a PMID
            if ( openurl.indexOf('rfr_id=info%3Asid%2Fprimo.exlibrisgroup.com') > -1 ) {
                if ( pmid ) {
                    module.resolve(`id=pmid:${pmid}`);
                }
            }

            var options = uresolver.get_options(openurl);
            log.debug('options calling uresolver');
            log.debug(options);

            var req = https.request(options, (uresolver_res) => {
                log.debug('uresolver statusCode:', uresolver_res.statusCode);

                uresolver_res.pipe(concat(function(buffer) {
                    var strXml = buffer.toString();


                    parser.parseString(strXml, async function(err, result) {

                        try {
                            log.debug("try parse");
                            log.debug(typeof result.uresolver_content.context_services[0].context_service);

                            
                            var cto = result.uresolver_content.context_object,
                                parsed_cto = JSON.parse(uresolver.CTO_to_JSON(cto));

                            // parse
                            // Identify false Crossref data
                            const regexPubDate = /^(\d{4})\-(\d{4})?/;
                            let matchPubDate;
                            // pubdate comes from Alma ^(\d{4})\-?(\d{4})?


                            /* If start of publication is later then the publication of the article
                               and we have a DOI we assume that the DOI is on the wrong publication */
                            if ( parsed_cto['rft.pubdate'] ) {
                                matchPubDate = parsed_cto['rft.pubdate'].match(regexPubDate);
                            }
                            
                            if ( parsed_cto['rft.pubdate'] && matchPubDate && matchPubDate[1] && Date.parse(matchPubDate[1]) > Date.parse(parsed_cto['rft.date']) ) {
                                log.debug('Publication start later than article publication');
                                let OpenURL = querystring.parse(openurl);
                                delete OpenURL['rft.doi'];
                                delete OpenURL.rft_id;
                                
                                module.resolve( querystring.stringify(OpenURL), true );
                                return;
                            }

                            /* If rft.pages isn't the same as spage and epage */
                            if ( parsed_cto['rft.spage'] && parsed_cto['rft.pages'] ) {
                                let pages = parsed_cto['rft.pages'];
                                
                                if ( parsed_cto['rft.epage'] ) {
                                    pages = `${parsed_cto['rft.spage']}-${parsed_cto['rft.epage']}`;
                                }

                                if ((pages !== parsed_cto['rft.pages']) || !parsed_cto['rft.pages'].startsWith(`${parsed_cto['rft.spage']}-`)) {
                                    log.debug('Pages not same as start page');
                                    console.log(parsed_cto);
                                    let OpenURL = querystring.parse(openurl);
                                    delete OpenURL['rft.pages'];
                                    delete OpenURL.pages;
                                    if ( parsed_cto['rft.jtitle'] ) {
                                        OpenURL['rft.jtitle'] = parsed_cto['rft.jtitle'];
                                    }

                                    module.resolve( querystring.stringify(OpenURL), true );
                                    return;
                                }
                            }

                            let title = String(parsed_cto['rft.title']).toLowerCase().trim().replace(/\s/g,'');
                            let atitle = String(parsed_cto['rft.atitle']).toLowerCase().trim().replace(/\s/g,'');
                            

                            if ( parsed_cto['rft.title'] && parsed_cto['rft.atitle'] &&
                                 title == atitle ) {
                                try {
                                    let page = `${parsed_cto['rft.issn'].replace('-','')}:${parsed_cto['rft.volume']}:${parsed_cto['rft.issue']}:${parsed_cto['rft.spage']}`;
                                    let undefValuesInPage = false;

                                    if (!(parsed_cto['rft.issn'] && parsed_cto['rft.volume'] && parsed_cto['rft.issue'] && parsed_cto['rft.spage'])) {
                                        undefValuesInPage = true;
                                    }

                                    if ( client.ping() ) {
                                        client.zRange(`articles:${page}`, 0, 3, (err, value) => {
                                            if (err) {
                                                console.log(err);
                                            }
                                            else {
                                                console.log('zrange value');
                                                console.log( value );
                                            }
                                    
                                            if ( 1 === value.length ) {
                                                log.debug('Considering one article');
                                                client.hgetall(`article:${page}:a`, (err, obj) => {
                                                    if (err)
                                                        console.log(err);

                                                    parsed_cto['rft.atitle'] = obj.title;
                                                    parsed_cto['rft.doi'] = obj.doi.trim();
                                                    openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);

                                                    if (undefValuesInPage) {
                                                        crossref.get_crossref_data_from_doi(parsed_cto['rft.doi'])
                                                            .then(function(crossref_data) {
                                                                let issn = parsed_cto['rft.issn'],
                                                                    vol = parsed_cto['rft.volume'],
                                                                    issue = parsed_cto['rft.issue'],
                                                                    spage = parsed_cto['rft.spage'],
                                                                    au = parsed_cto['rft.au'];

                                                                if (!issn) {
                                                                    log.debug('issing issn');
                                                                }
                                                                if (!vol) {
                                                                    log.debug('Missing volume');
                                                                    vol = crossref_data.journal[0].journal_issue[0].journal_volume[0].volume[0];
                                                                }
                                                                if (!issue) {
                                                                    log.debug('Missing issue');
                                                                    issue = crossref_data.journal[0].journal_issue[0].issue ? crossref_data.journal[0].journal_issue[0].issue[0] : '';
                                                                }
                                                                if (!spage) {
                                                                    log.debug('Missing spage');
                                                                    spage = crossref_data.journal[0].journal_article[0].pages[0].first_page[0];

                                                                }
                                                                if (!au) {
                                                                    log.debug('Missing first author');
                                                                    let contributor = crossref_data.journal[0].journal_article[0].contributors[0],
                                                                        person_name = contributor.person_name[0];
                                                                    

                                                                    if ( 'first' == person_name.$.sequence && 'author' && person_name.$.contributor_role ) {
                                                                        au = `${person_name.surname}, ${person_name.given_name}`;
                                                                        parsed_cto['rft.au'] = au;
                                                                    }
                                                                }
                                                                parsed_cto['rft.issn'] = issn;
                                                                parsed_cto['rft.volume'] = vol;
                                                                parsed_cto['rft.issue'] = issue;
                                                                parsed_cto['rft.spage'] = spage;

                                                                // Uppdatera openurl och nyckel i Redis
                                                                openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);

                                                                let old_page = page;
                                                                page = `${parsed_cto['rft.issn'].replace('-','')}:${parsed_cto['rft.volume']}:${parsed_cto['rft.issue']}:${parsed_cto['rft.spage']}`;
                                                                log.debug(`Changing ${old_page} to ${page}`);

                                                                client.renamenx(`article:${old_page}:a`, `article:${page}:a`, (err, res) => {
                                                                    if (err)
                                                                        console.error(err);
                                                                });

                                                                client.renamenx(`articles:${old_page}`, `articles:${page}`, (err, res) => {
                                                                    if (err)
                                                                        console.error(err);
                                                                });

                                                                if (au) {
                                                                    client.hset(`article:${page}:a`, 'au', au, redis.print);
                                                                }
                                                                
                                                                module.resolve(openurl);
                                                                
                                                            }).catch(function(error) {
                                                                console.error('Error fetching CrossRef data:', error);
                                                                // Fortsätt för att det inte ska hänga sig
                                                                openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);
                                                                module.resolve(openurl);

                                                            });
                                                    }
                                                    else {
                                                        module.resolve(openurl);
                                                    }
                                                    

                                                });
                                            }
                                            else if ( value.length > 0 ) {
                                                log.debug('More than one articles on this page');

                                                let i = 0;

                                                client.sort(`articles:${page}`, 'by', 'year', 'get',  '*->year', (err, arr) => {
                                                    if (err)
                                                        console.log(err);

                                                    for (i; i < arr.length; i++) {
                                                        if ( parsed_cto['rft.year'] == arr[i] ) {
                                                            client.sort(`articles:${page}`, 'by', 'year', 'get',  '#', (err, arr) => {
                                                                client.hGetAll(arr[i], (err, obj) => {
                                                                    console.log('Setting title to ', obj.title);
                                                                    
                                                                    parsed_cto['rft.atitle'] = obj.title;
                                                                    parsed_cto['rft.doi'] = obj.doi;

                                                                    openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);
                                                                    module.resolve(openurl);
                                                                });
                                                            });
                                                            break;
                                                        }
                                                    }

                                                    if ( i == arr.length ) {
                                                        log.debug('All stored articles have been considered, checking crossref');

                                                        client.sort(`articles:${page}`, 'ALPHA', 'desc', (err, arr) => {
                                                            if (err)
                                                                console.log(err);

                                                            let specifier = String.fromCharCode(arr[0].replace(/^(.*\:)([a-z])/,"$2").charCodeAt(0)+1);
                                                            let r_article = arr[0].replace(/^(.*)\:[a-z]/,"$1");

                                                            crossref.get_journal_article(uresolver.parsed_CTO_to_OpenURL(parsed_cto))
                                                                .then(function(journal_article) {
                                                                    let key = `${r_article}:${specifier}`;
                                                                    let article = crossref.parse_journal_article(journal_article);

                                                            
                                                                    log.debug('Corrects article title on parsed CTO');
                                                                    parsed_cto['rft.atitle'] = article.title;
                                                                    parsed_cto['rft.doi'] = article.doi;

                                                            
                                                                    log.debug(`Store as ${key}`);
                                                                    client.zadd(`articles:${page}`,
                                                                                '0',
                                                                                key, redis.print);

                                                                    client.hset(key, 'title', article.title, redis.print);
                                                                    client.hset(key, 'end_page', article.end_page, redis.print);
                                                                    client.hset(key, 'year', article.year, redis.print);
                                                                    client.hset(key, 'doi', article.doi, redis.print);

                                                                    openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);
                                                                    module.resolve(openurl);
                                                                    
                                                                })
                                                                .catch(function(error) {
                                                                    console.error('Error fetching journal article:', error);
                                                                    // Fortsätt
                                                                    openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);
                                                                    module.resolve(openurl);
                                                                });
                                                        });
                                                    }
                                                });
                                                
                                            }
                                            else {
                                                log.debug('No stored article in Redis cache');
                                                if (undefValuesInPage) {
                                                    log.debug('Could not find article but have undefined values in request');

                                                    let patt = `article:${page.substr(0,page.indexOf('undefined'))}*`;

                                                    client.scan('0', 'MATCH', patt, (err, res) => {
                                                        if (err)
                                                            console.error(err);

                                                        if ( 1 == res[1].length ) {
                                                            // ISSN and startpage already checked by the scan
                                                            client.hgetall(res[1][0], (err, hash) => {
                                                                if ( hash.year == parsed_cto['rft.year'] ) {

                                                                    let article_length = hash.end_page - parsed_cto['rft.spage'] + 1;

                                                                    if ( article_length > 0 && article_length < 49 ) {
                                                                        // ISSN, year and startpage is the same
                                                                        // Only one article stored in database
                                                                        // The article length is between 1 and 48 pages
                                                                        log.debug('Using the info that is stored in redis');
                                                                        log.debug('ISSN, year, startpage and article length used for decision');
                                                                        
                                                                        let re = new RegExp(`${patt.slice(0, -1)}(\\d+):(\\d+):${parsed_cto['rft.spage']}`);
                                                                        let match = res[1][0].match(re);

                                                                        parsed_cto['rft.volume'] = match[1];
                                                                        parsed_cto['rft.issue'] = match[2];
                                                                        parsed_cto['rft.atitle'] = hash.title;
                                                                        parsed_cto['rft.doi'] = hash.doi;
                                                                        
                                                                        if ( !parsed_cto['rft.au'] && hash.au ) {
                                                                            parsed_cto['rft.au'] = hash.au;
                                                                        }
                                                                        else if ( !parsed_cto['rft.au'] ) {
                                                                            // Author missing, try to set 
                                                                            crossref.get_crossref_data_from_doi(hash.doi)
                                                                                .then(function(crossref_data) {
                                                                                    let contributor = crossref_data.journal[0].journal_article[0].contributors[0],
                                                                                        person_name = contributor.person_name[0];
                                                                                    

                                                                                    if ( 'first' == person_name.$.sequence && 'author' && person_name.$.contributor_role ) {
                                                                                        client.hset(res[1][0], 'au', `${person_name.surname}, ${person_name.given_name}`, redis.print);
                                                                                    }
                                                                                }, function(e) {
                                                                                    console.error(e);
                                                                                });
                                                                        }
                                                                        else {
                                                                            console.log(parsed_cto);
                                                                        }
                                                                        
                                                                        openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);
                                                                        module.resolve(openurl, true);
                                                                    }
                                                                }
                                                            });
                                                        }
                                                        else if ( res[1].length > 1 )  {
                                                            log.debug('Cannot choose between several stored articles');
                                                        }
                                                        else {
                                                            log.debug('No stored articles, try to get it from crossref');

                                                            if ( parsed_cto['rft.atitle'] === parsed_cto['rft.title'] )
                                                                delete parsed_cto['rft.atitle'];
                                                            
                                                            crossref.get_journal_article(uresolver.parsed_CTO_to_OpenURL(parsed_cto))
                                                                .then(function(journal_article) {
                                                                    let key = `article:${page}:a`;
                                                                    let article = crossref.parse_journal_article(journal_article);
                                                                    
                                                                    log.debug('Corrects article title on parsed CTO');
                                                                    
                                                                    parsed_cto['rft.atitle'] = article.title;
                                                                    parsed_cto['rft.doi'] = article.doi;
                                                                    
                                                                    // Uppdatera openurl-varaibeln från korrigerad parsed_cto
                                                                    openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);

                                                                    log.debug('Storing article reference ', key);
                                                                    client.zadd(`articles:${page}`, '0', key, redis.print);
                                                                    
                                                                    client.hset(key, 'title', article.title, redis.print);
                                                                    client.hset(key, 'end_page', article.end_page, redis.print);
                                                                    client.hset(key, 'year', article.year, redis.print);
                                                                    client.hset(key, 'doi', article.doi, redis.print);

                                                                    module.resolve(openurl, true);
                                                                })
                                                                .catch(function(error) {
                                                                    console.error('Error fetching journal article:', error);
                                                                    // Forsätt
                                                                    openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);
                                                                    module.resolve(openurl);
                                                                });

                                                        }
                                                    });
                                                }
                                                else {
                                                    log.debug('No stored articles, try to get it from crossref');
                                                            
                                                    crossref.get_journal_article(uresolver.parsed_CTO_to_OpenURL(parsed_cto))
                                                        .then(function(journal_article) {
                                                            let key = `article:${page}:a`;
                                                            let article = crossref.parse_journal_article(journal_article);
                                                                    
                                                            log.debug('Corrects article title on parsed CTO');
                                                                    
                                                            parsed_cto['rft.atitle'] = article.title;
                                                            parsed_cto['rft.doi'] = article.doi;
                                                                    
                                                            // Uppdatera openurl-varaibeln från korrigerad parsed_cto
                                                            openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);

                                                            log.debug('Storing article reference ', key);
                                                            client.zadd(`articles:${page}`, '0', key, redis.print);
                                                                    
                                                            client.hset(key, 'title', article.title, redis.print);
                                                            client.hset(key, 'end_page', article.end_page, redis.print);
                                                            client.hset(key, 'year', article.year, redis.print);
                                                            client.hset(key, 'doi', article.doi, redis.print);

                                                            module.resolve(openurl, true);
                                                        })
                                                        .catch(function(error) {
                                                            console.error('Error fetching journal article:', error);
                                                            // Fortsätt
                                                            openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);
                                                            module.resolve(openurl);
                                                        });
l                                                }
                                            }
                                        });
                                    }
                                    else {
                                        console.log('Redis client is missing');
                                    }
                                }
                                catch (e) {
                                    console.error(e);
                                }
                            }
                            else if ( 'undefined' === parsed_cto['rft.volume'] ) {

                                console.log('undefined volume');

                                delete parsed_cto['rft.volume'];
                                delete parsed_cto['rft.date'];
                                delete parsed_cto['rft.year'];

                                const normalizedTitle = parsed_cto['rft.atitle'].toLowerCase().replace(/[\s\:\\.\?-]+/g,"");
                                crossref.questionInProgress = true;                                        

                                // Check if this title is cached
                                client.exists(`titles:${normalizedTitle}`, (err, i) => {
                                    if (err)
                                        console.log(err);

                                    if ( 0 === i ) {
                                        console.log(`titles:${normalizedTitle} isn't cached, try crossref`);
                                        
                                        // Try crossref if there is no cache
                                        console.log(parsed_cto);

                                        let issn = parsed_cto['rft.normalized_issn'] || parsed_cto['rft.issn']

                                        crossref.get_title_match(issn.replace('-',''), parsed_cto['rft.atitle'])
                                            .then(function(candidates) {

                                                console.log('Found title match');

                                                console.log( candidates );

                                                if ( 1 === candidates.length ) {
                                                    parsed_cto['rft.volume'] = candidates[0].volume;
                                                    parsed_cto['rft.year'] =  candidates[0].published['date-parts'][0][0];
                                                    parsed_cto['rft_id'] =  `doi:${candidates[0].DOI}`;
                                                    console.log('context is', context);

                                                    client.scard(`titles:${normalizedTitle}`, (err, card) => {
                                                        if (err)
                                                            console.log(err);

                                                        const a = 97
                                                
                                                        client.sadd(`titles:${normalizedTitle}`, String.fromCharCode(a + card), (err, obj) => {
                                                            if (err)
                                                                console.log(err);

                                                            client.hset(`titles:${normalizedTitle}:${String.fromCharCode(a+card)}`, 'volume', candidates[0].volume);
                                                            client.hset(`titles:${normalizedTitle}:${String.fromCharCode(a+card)}`, 'year', candidates[0].published['date-parts'][0][0]);
                                                            client.hset(`titles:${normalizedTitle}:${String.fromCharCode(a+card)}`, 'doi', candidates[0].DOI);
                                                        })
                                                    });

                                                    module.resolve(uresolver.parsed_CTO_to_OpenURL(parsed_cto), true);
                                                    crossref.questionInProgress = false;

                                                }
                                                else {
                                                    console.log( 'more than one title match, resolve without volume' );
                                                    
                                                    module.resolve(uresolver.parsed_CTO_to_OpenURL(parsed_cto), true);
                                                    crossref.questionInProgress = false;
                                                }
                                            }, function(reason) {
                                                console.log(reason);
                                                
                                                log.debug('Could not find title match from crossref');
                                                module.resolve(uresolver.parsed_CTO_to_OpenURL(parsed_cto), true);
                                                crossref.questionInProgress = false;

                                            });
                                    }
                                    else {
                                        console.log(`titles:${normalizedTitle} is cached`);


                                        // Get volume and year from cache
                                        client.scard(`titles:${normalizedTitle}`, (err, card) => {
                                            if (err)
                                                console.log(err);

                                            console.log( `Cardinality for ${normalizedTitle}:`, card );

                                            if ( 1 === card ) {
                                                client.hgetall(`titles:${normalizedTitle}:a`, (err, obj) => {
                                                    if (err)
                                                        console.log(err);

                                                    if ( obj.volume ) 
                                                        parsed_cto['rft.volume'] = obj.volume;

                                                    if ( obj.year )
                                                        parsed_cto['rft.year'] = obj.year;

                                                    if ( obj.doi )
                                                        parsed_cto['rft_id'] = `doi:${obj.doi}`;

                                                    module.resolve(uresolver.parsed_CTO_to_OpenURL(parsed_cto), true);
                                                    crossref.questionInProgress = false;
                                                    
                                                });
                                            }
                                            else {
                                                // TODO: discriminate
                                                console.log('could not discriminate redis cache');
                                                // Resolve without volume and year
                                                module.resolve(uresolver.parsed_CTO_to_OpenURL(parsed_cto), true);
                                                crossref.questionInProgress = false;
                                            }
                                        });
                                    }
                                });
                            }

                            var today = Date.now();

                            if ( uresolver.service_exist(result.uresolver_content) ) {
                                                  
                                services = uresolver.services_to_JSON(result.uresolver_content.context_services);
                                log.debug('services to check: '+services.length);

                                var free_service = services.find(uresolver.service.is_free),
                                    our_service = services.find(uresolver.service.is_our),

                                    gin_service = services.find(uresolver.service.is_gin),
                                    other_service = services.find(uresolver.service.is_other);
                                
                                our_services = services.filter(uresolver.service.is_our);

                                if ( free_service ) {
                                    if ( our_service ) {
                                        if ( free_service === our_service ) {
                                            log.debug('Our service is free');

                                            if ( free_service.key.Filtered && !other_service ) {
                                                log.debug('but filtered');
                                                if ( false === crossref.questionInProgress ) {
                                                    log.debug(our_service.key['Filter reason'] + ' redirecting to ill form');
                                                    _redirect(conf.url.illForm+'?'+openurl);
                                                }
                                                else {
                                                    console.log( 'Wait for crossref');
                                                    console.log( crossref.questionInProgress );
                                                }
                                            }
                                            else if ( free_service.key.Filtered && gin_service && !other_service ) {
                                                log.debug('but filtered so we try Get It Now');
                                                _handle_service(gin_service,openurl,cto,self);
                                            }
                                            else {
                                                if ( false === crossref.questionInProgress ) {
                                                    _handle_service(our_service,openurl,cto,self);
                                                }
                                                else {
                                                    console.log( 'Wait for crossref');
                                                    console.log( crossref.questionInProgress );
                                                }
                                            }
                                        }
                                        else {
                                            var aveUntil = our_service.key['Availability'] ? Date.parse( our_service.key['Availability'].replace(/.*(until|till) (\d{4}-\d{2}-\d{2}).*/g,"$2") ) : undefined,
                                                pubTime;
                                            
                                            if ( parsed_cto['rft.year'] && parsed_cto['rft.month'] ) {
                                                pubTime = Date.parse(parsed_cto['rft.year']+'-'+parsed_cto['rft.month']);
                                            }
                                            else if ( "Elsevier" === parsed_cto['rft.publisher'] ) {
                                                log.debug('Elsevier service, but we don\'t know the month');
                                            }

                                            if ( our_service.key['Filter reason'] === "Date Filter" || ( pubTime && aveUntil && ( pubTime > aveUntil ) ) ) {
                                                log.debug('We have a service but it has emabargo or only backfiles. Try to find OA');

                                                // Sätt openurl från parsed_cto
                                                openurl = uresolver.parsed_CTO_to_OpenURL(parsed_cto);
                                                console.log(openurl);

                                                _find_oa(openurl, parsed_cto['rft.doi'])
                                                    .then(function(url) {
                                                        log.debug('redirect to oa url');
                                                        _redirect(url, cto);
                                                    }, function(reason) {
                                                        log.debug(reason);

                                                        console.log( reason.data )
                                                        if ( reason.data ) {
                                                            console.log('sista stund');
                                                            console.log( JSON.parse( reason.data ) );
                                                        }

                                                        if ( gin_service && _service_from_proxy(gin_service) ) {
                                                            log.debug('Using Get It Now instead of our service that has embargo or only backfiles' + our_service.key['Filter reason']);
                                                            _redirect(gin_service.resolution_url);
                                                        }
                                                        else {
                                                            log.debug(our_service.key['Filter reason'] + ' redirecting to ill form');
                                                            _redirect(conf.url.illForm+'?'+openurl);
                                                        }
                                                    });
                                            }
                                            else {
                                                if ( our_service ) {
                                                    log.debug("Try our service before any free one");
                                                    log.debug(our_service);
                                                    _handle_service(our_service,openurl,cto,self);
                                                }
                                                else {
                                                    log.debug("There's another service (not ours) that is free. Try that one");
                                                    _handle_service(free_service,openurl,cto,self);
                                                }
                                            }
                                        }
                                    }
                                    else {
                                        log.debug("We don't have a service. Check if we can find OA");
                                        
                                        let doi = uresolver.get_identifier_from_CTO(cto, 'doi');
                                        let pmid = uresolver.get_identifier_from_CTO(cto, 'pmid');

                                        _find_oa(openurl, doi, pmid)
                                            .then(function(url) {
                                                log.debug('redirect to oa url');
                                                _redirect(url, cto);
                                            }, function(reason) {
                                                log.debug(reason);
                                                log.debug('redirect to the ill form');
                                                _redirect(conf.url.illForm+'?'+openurl);
                                            });
                                    }
                                }
                                else if ( our_service ) { 
                                    log.debug("We have a service that's not free or is in embargo");

                                    if ( our_service.key['Filter reason'] === "Date Filter" ) {
                                        log.debug('We have a non-free service but it has emabargo');

                                        log.debug('Try to find OA');
                                            
                                        _find_oa(openurl, parsed_cto['rft.doi'])
                                            .then(function(url) {
                                                log.debug('redirect to oa url');
                                                _redirect(url, cto);
                                            }, function(reason) {
                                                log.debug(reason);

                                                if ( reason && reason.data ) {
                                                    let unpaywall = JSON.parse( reason.data );

                                                    if ( 'journal-article' === unpaywall.genre && 'article' === parsed_cto['rft.genre'] ) {
                                                        if ( parsed_cto['rft.atitle'] === parsed_cto['rft.title'] ) {
                                                            parsed_cto['rft.atitle'] = unpaywall.title; // Todo: also fix openurl
                                                        }
                                                    }
                                                }

                                                if ( gin_service && _service_from_proxy(gin_service) ) {
                                                    GetItNow.get_target_url(parsed_cto,gin_service)
                                                        .then(function(target_url) {
                                                            log.debug('Using Get It Now instead of "our service", ' + our_service.key['Filter reason']);
                                                            _redirect(conf.url.proxyPrefix + target_url);
                                                        }, function(reason) {
                                                            log.debug(reason);
                                                            log.debug('redirect to the ill form');
                                                            _redirect(conf.url.illForm+'?'+cto_helper.to_openurl(parsed_cto));
                                                        });
                                                }
                                                else {
                                                    log.debug(our_service.key['Filter reason'] + ' redirecting to ill form');
                                                    _redirect(conf.url.illForm+'?'+openurl);
                                                }

                                            });
                                    }
                                    else {
                                        log.debug('Handling our service');
                                        
                                        if ( false === crossref.questionInProgress ) {
                                            console.log('No crossref question');
                                            _handle_service(our_service,openurl,cto,self);
                                        }
                                        else {
                                            console.log( 'Wait for crossref');
                                            console.log( crossref.questionInProgress );
                                        }
                                    }
                                }
                                else {
                                  log.debug("We don't have a service, and Alma doesn't know any free. Check if we can find OA anyway");

                                  if (parsed_cto['rft.pmid'] ) {
                                    let pmid = parsed_cto['rft.pmid'];

                                    var Elink = require('./pubmed/elink.js');
                                    var elink = new Elink(pmid);

                                    elink.freeUrl()
                                        .then(function(link) {
                                            log.debug('using free url from pubmed for a source we don\'t have');
                                            _redirect(link,cto);
                                        }, function(reason) {
                                            log.debug(reason);
                                            log.debug('redirecting to ill form');
                                            _redirect(conf.url.illForm+'?'+openurl);
                                        });
                                  }
                                  else {
                                    _find_oa(openurl, parsed_cto['rft.doi'], parsed_cto['rft.pmid'])
                                      .then(function(url) {
                                        log.debug('redirect to oa url');
                                        _redirect(url, cto);
                                      }, function(reason) {
                                        if (reason) {
                                          log.debug(reason);
                                        }
                                        
                                        // Correct article title if neccessary
                                        if ( reason && reason.data && parsed_cto['rft.atitle'] ) {
                                          parsed_cto['rft.atitle'] = JSON.parse(reason.data).title;
                                        }

                                        var service_from_proxy = services.find(_service_from_proxy);

                                        if ( service_from_proxy ) {
                                          log.debug('Selected '+service_from_proxy.key['parser_program']+' from proxy');

                                          var ava = service_from_proxy.key['Availability'];

                                          // Don't allow Get It Now if there's other services
                                          /* Get It Now required volume to handle a request, but
                                             we can now create a request if we use our own parser */
                                          if ( "CCC::GIN" === service_from_proxy.key['parser_program'] && services.length > 1 || "undefined" === typeof parsed_cto['rft.volume'] ) {
                                            GetItNow.get_target_url(parsed_cto,service_from_proxy)
                                              .then(function(target_url) {
                                                log.debug('Using Get It Now without volume');
                                                _redirect(conf.url.proxyPrefix + target_url);
                                              }, function(reason) {
                                                log.debug(reason);
                                                log.debug('redirect to the ill form');
                                                _redirect(conf.url.illForm+'?'+cto_helper.to_openurl(parsed_cto));
                                              });
                                          }
                                          else if ( ava ) {
                                            // we need to check the availability key since the date filter
                                            // doesn't work outside the ip range of the campus
                                            log.debug('checking date for proxy service');

                                            var avaMatch = ava.match(/Available from (\d{4})( volume: (\d*))?( issue: (\d*))?( until (\d{4})( volume: (\d*))?( issue: (\d*))?)?/i);

                                            if (!avaMatch) {
                                              avaMatch = ava.match(/Tillgänglig från (\d{4})( volym: (\d*))?( nummer: (\d*))?/i);
                                                    }
                                                    
                                            var availability = {
                                              from: {},
                                              to: {}
                                            };

                                            if (avaMatch) {
                                              if ( avaMatch[1] ) { availability.from.year = avaMatch[1]; }
                                              if ( avaMatch[3] ) { availability.from.volume = avaMatch[3]; }
                                              if ( avaMatch[5] ) { availability.from.issue = avaMatch[5]; }
                                              if ( avaMatch[7] ) { availability.to.year = avaMatch[7]; }
                                              if ( avaMatch[9] ) { availability.to.volume = avaMatch[9]; }
                                              if ( avaMatch[11] ) { availability.to.volume = avaMatch[11]; }
                                                        
                                              if ( availability.from.year && ( parsed_cto['rft.year'] >= availability.from.year ) ) {
                                                if ( availability.to.year && ( parsed_cto['rft.year'] > availability.to.year ) ) {
                                                  _redirect(conf.url.illForm+'?'+openurl);
                                                }
                                                else {
                                                  if ( "CCC::GIN" === service_from_proxy.key['parser_program'] ) {
                                                    log.debug('Using Get It Now for this request');
                                                    log.debug(module._OPEN_URL_REQUEST);
                                                  }
                                                  // Use our own parser, we need to parse again
                                                  GetItNow.get_target_url(parsed_cto, service_from_proxy)
                                                    .then( (target_url) => {
                                                      _redirect(conf.url.proxyPrefix + target_url);
                                                    }, function(reason) {
                                                      log.debug(reason);
                                                      log.debug('redirect to the ill form');
                                                      _redirect(conf.url.illForm+'?'+cto_helper.to_openurl(parsed_cto));
                                                    });
                                                }
                                              }
                                              else {
                                                log.debug('redirecting to ill form');
                                                _redirect(conf.url.illForm+'?'+openurl);
                                              }
                                            }
                                            else {
                                              _redirect(service_from_proxy.resolution_url);
                                            }
                                          }
                                          else {
                                            log.debug('redirecting to ill form');
                                            _redirect(conf.url.illForm+'?'+openurl);
                                          }
                                        }
                                        else {
                                          log.debug('redirecting to ill form');
                                          _redirect(conf.url.illForm+'?'+openurl);
                                        }
                                      });
                                  }
                                }
                            }
                          else {
                                log.debug("didn't find any service"); // SWEPUB

                                if ( parsed_cto['rft.pmid'] ) {
                                    let pmid = parsed_cto['rft.pmid'];

                                    var Elink = require('./pubmed/elink.js');
                                    var elink = new Elink(pmid);

                                    elink.freeUrl()
                                        .then(function(link) {
                                            log.debug('using free url from pubmed for a source we don\'t have');
                                            _redirect(link,cto);
                                        }, function(reason) {
                                            log.debug(reason);
                                            log.debug('redirecting to ill form');
                                            _redirect(conf.url.illForm+'?'+openurl);
                                        });
                                            
                                }
                                else if ( parsed_cto['rft.isbn'] && 'dissertation' == parsed_cto['rft.genre'] ) {
                                    import('./helpers/utils/isbn.util.js')
                                        .then( (module) => {
                                            const { Isbn } = module;
                                            const isbn = new Isbn(parsed_cto['rft.isbn']);

                                            if ( 'sweden' == isbn.countryOfPublication.toLowerCase() ) {
                                                import('./helpers/utils/libris.util.js')
                                                    .then( (module) => {
                                                        const { LibrisXSearch } = module;
                                                        const swepub = new LibrisXSearch('swepub');
                                                        const libris = new LibrisXSearch('libris');
                                                        swepub.getFulltextLinks( `NUMM:${isbn.number}` )
                                                            .then( (swepubLinks) => {
                                                                _redirect(swepubLinks[0].url, cto, false, false, {}, true);
                                                            })
                                                            .catch( (swepubError) => {
                                                                console.error('Error in Swepub:', swepubError);
                                                                libris.getFulltextLinks( `NUMM:${isbn.number}` )
                                                                    .then( (librisLinks) => {
                                                                        _redirect(librisLinks[0].url, cto, false, false, {}, true);
                                                                    })
                                                                    .catch( (librisError) => {
                                                                        console.error('Error in Libris:', librisError);
                                                                        _redirect(conf.url.illForm+'?'+openurl);
                                                                    });
                                                            });
                                                    });
                                            }
                                        });
                                }
                                else if ( 'dissertation' == parsed_cto['rft.genre'] || ( parsed_cto.rft_dat && parsed_cto.rft_dat.includes('swepub') ) ) {
                                    if ( parsed_cto.rft_dat ) {
                                        try {
                                            parser.parseString( parsed_cto.rft_dat, (err, dat) => {
                                                let swepubKey = Object.keys(dat).filter(k => k.startsWith('swepub'));

                                                if ( swepubKey ) {
                                                    import('./helpers/utils/libris.util.js')
                                                        .then( (module) => {
                                                            const { LibrisXSearch } = module;
                                                            const swepub = new LibrisXSearch('swepub');

                                                            swepub.getFulltextLinks( dat[swepubKey] )
                                                                .then( (swepubLinks) => {
                                                                    _redirect(swepubLinks[0].url, cto, false, false, {}, true);
                                                                })
                                                                .catch( (swepubError) => {
                                                                    console.error('Error in Swepub:', swepubError);
                                                                    _redirect(conf.url.illForm+'?'+openurl);
                                                                });;
                                                        });
                    
                                                }

                                            });
                                        }
                                        catch (error) {
                                            console.error(error);
                                            _redirect(conf.url.illForm+'?'+openurl);
                                        }
                                    }
                                    else {
                                        _redirect(conf.url.illForm+'?'+openurl);
                                    }
                                }
                                else {
                                    // Is there a DOI and no ISBN, check the DOI
                                    if ( parsed_cto['rft.doi'] && !parsed_cto['rft.isbn'] ) {
                                        let isbn = parsed_cto['rft.doi'].replace(/^10.\d{4,9}\/.*(\d{13}).*$/,"$1");
                                        var isbnHelper = require('./helpers/isbn.js');

                                        if ( isbnHelper.isbn13_valid(isbn) ) {
                                            parsed_cto['rft.isbn'] = isbn;
                                            module.resolve(uresolver.parsed_CTO_to_OpenURL(parsed_cto), true);
                                            return;
                                        }
                                    }

                                    // Try to find OA
                                    _find_oa(openurl, parsed_cto['rft.doi'], uresolver.get_identifier_from_CTO(cto, 'pmid'))
                                        .then(function(url) {
                                            log.debug('redirect to oa url');
                                            _redirect(url, cto, true);
                                        }, function(reason) {
                                            log.debug(reason);
                                            log.debug('redirecting to ill form');
                                            _redirect(conf.url.illForm+'?'+openurl);
                                        });
                                }
                            }
                        }
                        catch (err) {
                            log.error(err);
                        }
                            

                    });

                                       
                }));
            });

            req.on('error', (e) => {
                log.error(e);
            });
            req.end();

        }
        else {
            log.error('Missing openurl');
            _redirect(conf.url.servicePage);
        }

        parser.on('error', function(err) { log.error('Parser error' - err); });
    };

    function _clean_openurl(openurl) {
        // <date>: [YYYY-MM-DD|YYYY-MM|YYYY]
        var numerals = require('./helpers/numerals.js');

        // N.B. We  need to force direct so we get the target url
        var obj = querystring.parse(openurl);

        delete obj.svc_dat;
        delete obj.ctx_id;
        delete obj.Incoming_URL;
        delete obj.customer;
        delete obj.licenseEnable;

        if ( obj.date && isNaN(Date.parse(obj.date)) && false === isNaN(obj.date.replace(/.*\s(\d{4})$/,"$1")) ) {
            obj.date = obj.date.replace(/.*\s(\d{4})$/,"$1");
        }

        if ( obj['rft.spage'] && isNaN(obj['rft.spage']) && 0 === numerals.from_roman(obj['rft.spage']) ) {
            delete obj['rft.spage'];
            delete obj['rft.pages'];
        }

        var romanJournals = /0016-?1128/i;

        if (obj.volume) {
            if ( numerals.from_roman(obj.volume) > 0 && false === romanJournals.test(obj.issn) ) {
                obj.volume = numerals.from_roman(obj.volume);
            }
            else if (/0016-?1128/i.test(obj.issn)) {
                obj.volume = numerals.to_roman(obj.volume);
            }
        }
        else if (obj['rft.volume']) {
            if ( numerals.from_roman(obj['rft.volume']) > 0 && false === romanJournals.test(obj['rft.issn']) ) {
                obj['rft.volume'] = numerals.from_roman(obj['rft.volume']);
            }
            else if (romanJournals.test(obj['rft.issn'])) {
                obj['rft.volume'] = numerals.to_roman(obj['rft.volume']);
            }
        }

        return querystring.stringify(obj);
    }

    function _get_context_key(keys, id) {
        var i = 0;
        while ( i < keys.length ) {
            if ( id === keys[i].$.id ) {
                return keys[i]._;
            }
            i++;
        }
        return;
    }

    function _get_doi(openurl, context_object) {
        var q = querystring.parse( openurl ),
            doi;

        if ( q.id && true === /^doi:/gi.test(q.id) ) {
            doi = q.id.substr(4);
        }
        else if ( q.rft_id ) {
            if ( typeof q.rft_id === "object" ) {
                q.rft_id.forEach(function(rft_id) {
                    if ( true === /^info:doi\//gi.test(rft_id) ) {
                        doi = rft_id.substr(9);
                    }
                });
            }
            else if ( typeof q.rft_id === "string" && true === /^info:doi\//gi.test(q.rft_id) ) {
                doi = q.rft_id.substr(9);
            }
        }

        if ( !doi && context_object ) {
            doi = _get_context_key(context_object[0].keys[0].key,"rft.doi");
        }

        return doi;
    }

    function _get_pmid(openurl, context_object) {
        let q = querystring.parse( openurl ),
            pmid;

        if ( Array.isArray( q.id ) ) {
            pmid = q.id.find( i => i.startsWith('pmid:') );
            pmid = pmid ? pmid.substr(5) : '';
        }
        else if ( q.id && true === /^pmid:/gi.test(q.id) ) {
            pmid = q.id.substr(5);
        }
        else if ( q.rft_id ) {
            if ( typeof q.rft_id === "object" ) {
                q.rft_id.forEach(function(rft_id) {
                    if ( true === /^info:pmid\//gi.test(rft_id) ) {
                        pmid = rft_id.substr(10);
                    }
                });
            }
            else if ( typeof q.rft_id === "string" && true === /^info:pmid\//gi.test(q.rft_id) ) {
                pmid = q.rft_id.substr(9);
            }
        }

        if ( !pmid && context_object ) {
            pmid = _get_context_key(context_object[0].keys[0].key,"rft.pmid");
        }

        return pmid;
    }

    function _handle_service(service,openurl,cto,selfResolved) {
        var url = require('url');

        /* The link with the highest priority has a target url,
           if the link doesn't have a target url try to find an oa location
           before redirecting to resolution url 
        */
        var oCTO = JSON.parse(uresolver.CTO_to_JSON(cto));



        if ( oCTO.retracted && oCTO['rft.doi'] && conf.libkey.io && conf.libkey.library ) {
            _redirect(`${conf.libkey.io}/libraries/${conf.libkey.library}/${oCTO['rft.doi']}`, cto, false);
        }
        else if ( service.target_url ) {
            // check for the link in pubmed if the article is free an we have a pmid

            var pmid = '';
            if ( Array.isArray( oCTO.rft_id ) ) {
                pmid = oCTO.rft_id.find(function(element) {
                    return element.toLowerCase().startsWith("pmid:");
                });
                pmid = pmid ? pmid.match(/^pmid:(\d*)/)[1] : null;
            }
            else if ( "string" === typeof oCTO.rft_id && oCTO.rft_id.toLowerCase().startsWith("pmid:") ) {
                pmid = oCTO.rft_id.toLowerCase().match(/^pmid:(\d*)$/)[1];
            }

            if ( pmid ) {
              var Elink = require('./pubmed/elink.js');
              var elink = new Elink(pmid);

                // Is_free needs to be a number
                if ( Number(service.key.Is_free) ) {
                    elink.freeUrl()
                        .then(function(link) {
                            log.debug('using url from pubmed for free fulltext source that we have a service for');
                            _redirect(link, cto, true, true, {}, true);
                        }, function(reason) {
                            log.debug(reason);
                            _find_oa(openurl, oCTO['rft.doi'])
                                .then(function(link) {
                                    log.debug('redirect to oa url');
                                    _redirect(link, cto);
                                }, function(reason) {
                                    log.debug(reason);

                                    
                                    // don't do the HEAD check again 
                                    if ( selfResolved ) {
                                        _handle_target_service(service,openurl,cto,false);
                                    }
                                    else {
                                        _handle_target_service(service,openurl,cto);
                                    }
                                });
                        });
                }
                else {
                    elink.prUrl()
                        .then(function(objurl) {
                            log.debug('Got publisher URL from Pubmed');
                            var link = objurl.url.value;
                            var serviceTarget = url.parse(service.target_url),
                                target_url = serviceTarget.hostname.endsWith(conf.url.proxyHost) ? url.parse(serviceTarget.search.substring(serviceTarget.search.indexOf('http'))) : serviceTarget;

                            if ( url.parse(link).hostname === url.parse(target_url).hostname || service.key.interface_name.includes(objurl.provider.name) )
                            {
                                log.debug('Redirecting to Pubmed URL');
                                _redirect(conf.url.proxyPrefix+link, cto, false);
                            }
                            else {
                                log.debug('pubmed and alma does not have the same interface this service');
                                _handle_target_service(service,openurl,cto);
                            }
                        }, function(reason) {
                            log.debug(reason);
                            // We don't want to check Pubmed again, so set efetch to false
                            _handle_target_service(service,openurl,cto,true,false);
                        });
                }
            }
            else {

                let doi = oCTO['rft.doi'] || service.target_url.replace(/^.*(10\.\d{4,9}\/[-._;()\/:A-Z0-9]+).*$/i,"$1");



                if ( doi ) {
                    libkey.getRetractionNoticeUrl(doi)
                        .then( ( retractionNoticeUrl ) => {
                            _redirect( retractionNoticeUrl );
                        }, ( reason ) => {
                            // If the service is free and libkey has contentLocation, redirect
                            if ( service.key.Is_free > 0 && reason.data && reason.data.unpaywallUsable ) {
                                console.log( service );
                                _redirect( reason.data.contentLocation );
                            }
                            else {
                                _handle_target_service(service,openurl,cto);
                            }
                        });

                }
                else {
                    _handle_target_service(service,openurl,cto);
                }
            }

        }
        else if ( 'journal' === oCTO['rft.genre'] ) {
            log.debug('using resolution url for journal');
            _redirect(service.resolution_url);
        }
        else {
            log.debug('the service does not have a target url');

            // Does we have a PMID och DOI?

            if ( 'swepub' === service.key.request_source ) {                log.debug('using resolution url for swepub');
                                                                            _redirect(service.resolution_url);
                                                           } else {
                                                               var my_cto = cto_helper.my_cto(cto);

                                                               // check the DOI
                                                               if ( oCTO['rft.doi'] ) {
                                                                   _check_URL(`https://doi.org/${oCTO['rft.doi']}`)
                                                                       .then(function (response) {
                                                                           let doi = '';

                                                                           if ( 302 === response.statusCode ) {
                                                                               // Publishers DOI resolvers
                                                                               let domain = url.parse( response.headers.location ).host.replace('www.','').replace(/(.*)\.(.*)/g,"$1");
                                                                               let topdomain = url.parse( response.headers.location ).host.replace('www.','').replace(/(.*)\.(.*)/g,"$2");
                            
                                                                               if ( service.key.parse_parameters.indexOf(
                                                                                   url.parse(response.headers.location).host.replace('www.','')
                                                                               ) > -1
                                                                                    || ( service.key.parse_parameters.indexOf(domain) > - 1 &&
                                                                                         service.key.parse_parameters.indexOf(topdomain) > - 1 )
                                                                                  ) {
                                                                                   _redirect(conf.url.proxyPrefix+
                                                                                             response.headers.location,
                                                                                             cto);
                                                                                   return;
                                                                               }
                                                                           }
                                                                           else if ( 301 === response.statusCode ) {
                                                                               // Unsure about other status codes, e.g. Wiley
                                                                               doi = url.parse( response.headers.location ).path.substring(1);
                                                                           }
                                                                           else if ( 200 === response.statusCode ) {
                                                                               doi = oCTO['rft.doi'];
                                                                           }

                                                                           if ( doi ) {
                                                                               crossref.get_resource_url_from_doi(doi)
                                                                                   .then(function(resource_url) {
                                                                                       let parsedResourceURL = url.parse(resource_url),
                                                                                           our_doi = our_services.findIndex(service => service.key.parse_parameters.includes(parsedResourceURL.host));

                                                                                       if (our_doi > -1) {
                                                                                           log.debug('url from doi is on the same host so we redirect');
                                                                                           _redirect(conf.url.proxyPrefix+resource_url, cto);
                                                                                       }
                                                                                       else {
                                                                                           _find_oa(openurl, doi)
                                                                                               .then(function(url) {
                                                                                                   log.debug('redirect to oa url');
                                                                                                   _redirect(url, cto);
                                                                                               }, function(reason) {
                                                                                                   log.debug(reason);
                                                                                                   if ( _service_from_proxy(service) ) {
                                                                                                       log.debug('using resolution url from service selected by proxy');
                                                                                                       _redirect(service.resolution_url);
                                                                                                   }
                                                                                                   else {
                                                                                                       _redirect(conf.url.illForm+'?'+cto_helper.to_openurl(my_cto));
                                                                                                   }
                                                                                               });
                                                                                       }
                                                                                   }, function(reason) {
                                                                                       if ( ['200', '301', '302'].includes(response.statusCode) ) {
                                                                                           _redirect(service.resolution_url);
                                                                                       }
                                                                                       else {
                                                                                           log.debug(reason);
                                                                                           log.debug(`Building error page, DOI gave ${response.statusCode}`);
                                                                                           let errorpage = require('./error_page.js');
                                                                                           errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                                                                       }
                                                                                   });
                                                                           }
                                                                           else {
                                                                               log.debug('We don\'t have DOI');
                                                                               log.debug(`Building error page, response gave ${response.statusCode}`);
                                                                               let errorpage = require('./error_page.js');
                                                                               errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                                                           }
                                                                       });
                                                               }
                                                               else {
                                                                   log.debug('Try to get a DOI');
                                                                   crossref.get_resource_url_candidates(cto_helper.to_openurl(my_cto))
                                                                       .then(function(candidates) {
                                                                           if ( candidates.length === 1 ) {
                                                                               let redirectUrl = service.key.Is_free ? candidates[0] : conf.url.proxyPrefix+candidates[0];
                                                                               _redirect(redirectUrl);
                                                                           }
                                                                           else {
                                                                               log.debug('Several candidates');
                                                                               console.log(candidates);
                                                                               _redirect(conf.url.illForm+'?'+cto_helper.to_openurl(my_cto));
                                                                           }

                                                                       }, function(reason) {
                                                                           console.error(reason);

                                                                           if ( _service_from_proxy(service) ) {
                                                                               log.debug('using resolution url from service selected by proxy');
                                                                               _redirect(service.resolution_url);
                                                                           }
                                                                           else if ( service.key.Is_free ) {
                                                                               log.debug('using resolution url from free service');
                                                                               _redirect(service.resolution_url);
                                                                           }
                                                                           else {
                                                                               log.debug('no proxy service');
                                                                               _redirect(conf.url.illForm+'?'+cto_helper.to_openurl(my_cto));
                                                                           }
                                                                       });
                                                               }
                                                           }
        }
    }

    function _handle_target_service(service,openurl,cto,head,efetch) {
        console.log('VI HAR ETT MÅL');
        var oCTO = JSON.parse(uresolver.CTO_to_JSON(cto));

        if ( /[a-z]/i.test(oCTO['rft.issue']) && service.target_url.indexOf(oCTO['rft.issue']) > -1 && "undefined" === typeof oCTO['rft.doi'] ) {
            log.debug('issue is used in linking and contains letters');
            log.debug('there is no doi, so try crossref');
                            
            crossref.get_resource_url(openurl)
                .then(function(url) {
                    _redirect(conf.url.proxyPrefix+url, cto);
                }, function(reason) {
                    log.debug(reason);
                    _resolve_service(service,openurl,cto,head,efetch);
                });
        }
        else {
            _resolve_service(service,openurl,cto,head,efetch);
        }
    }

    function _service_from_proxy(service) {
        return service.key.proxy_selected === conf.proxyName;
    }


    /**
     * Resolver functions
     */
    function _find_oa(openurl, doi, pmid) {
        return new Promise(async (resolve, reject) => {
            if ( doi ) {
                oadoi.get_oa_location( doi ).then(function(url) {
                    _check_URL(url)
                        .then(function(response) {
                            console.log(`statusCode: ${response.statusCode}`);
                            if ( [303].includes(response.statusCode) ) {
                                reject('false oa');
                            }
                            else {
                                resolve(url);
                            }
                        }, function(req) {
                            if (pmid) {
                                log.debug('unpaywall didn\'t pass URL check. Fetching link from Pubmed insteda');
                                var Elink = require('./pubmed/elink.js');
                                var elink = new Elink(pmid);

                                elink.freeUrl()
                                    .then(function(link) {
                                        log.debug('tried unpaywall, but got link from pubmed');
                                        resolve(link);
                                    }, function(reason) {
                                        log.debug('neither unpaywall or pubmed has a working link');
                                        resolve(url);
                                    });
                                
                            }
                            else {
                                log.debug('unpaywall\'s link does not seem to work');
                                log.debug(url);
                                resolve(url);
                            }
                        });
                }, function(reason) {
                    var Elink = require('./pubmed/elink.js');
                    // Does Crossref have any license information? 
                    crossref.filter_license(doi)
                        .then(function (obj) {
                            try {
                                let license_CC = obj.message.items[0].license.find(function (license) {
                                    return 'vor' === license['content-version'] && license.URL.includes('creativecommons.org');
                                });

                                if (license_CC) {
                                    log.debug('Crossref has a CC license');
                                    resolve(conf.url.proxyPrefix+obj.message.items[0].URL);
                                }
                                else {
                                    log.debug('Crossref doesn\t contain any free license');
                                    reject();
                                }
                            }
                            catch (err) {


                                if (!pmid && doi) {
                                    var IdConv = require('./pubmed/idconv.js');
                                    var idconv = new IdConv(doi);

                                    idconv.getResults()
                                        .then(function(response) {
                                            if ( "ok" === response.status && 1 === response.records.length ) {
                                                let elink = new Elink(response.records[0].pmid);

                                                elink.freeUrl()
                                                    .then(function(link) {
                                                        log.debug('using url from pubmed for free fulltext source');
                                                        resolve(link);
                                                    }, function(reason) {
                                                        reject(reason);
                                                    });
                                            }
                                        }, function(reason) {
                                            reject(err);
                                        });
                                }
                                else {
                                    reject(err);
                                }
                            }
                        }, function(err) {
                            log.debug('couldn\'t find free resource from doi');

                            if (!pmid && doi) {
                                var IdConv = require('./pubmed/idconv.js');
                                var idconv = new IdConv(doi);

                                idconv.getResults()
                                    .then(function(response) {
                                        if ( "ok" === response.status && 1 === response.records.length ) {
                                            let elink = new Elink(response.records[0].pmid);

                                            elink.freeUrl()
                                                .then(function(link) {
                                                    log.debug('using url from pubmed for free fulltext source');
                                                    resolve(link);
                                                }, function(reason) {
                                                    reject(reason);
                                                });
                                        }
                                    }, function(reason) {
                                        reject(err);
                                    });
                            }
                            else {
                                reject(err);
                            }
                        });
                });
            }
            else {
                let a = querystring.parse(openurl);
                console.log('Get all', `article:${a.issn}:${a.volume}:${a.issue}:${a.spage}:a`);
                // Redis isn't in legacy mode here
                let data = await client.HGETALL(`article:${a.issn}:${a.volume}:${a.issue}:${a.spage}:a`);
                if ( Object.keys(data).length ) {
                    oadoi.get_oa_location( JSON.parse(data).doi )
                        .then(function(url) {
                            log.debug('got OA location from stored DOI');
                            resolve(url);
                        }, function(reason) {
                            log.debug('couldn\'t find free resource from openurl');
                            reject(reason);
                        });
                }
                else {
                    oadoi.get_oa_location_by_openurl( openurl )
                        .then(function(url) {
                            log.debug('got OA location from openurl');
                            resolve(url);
                        }, function(reason) {
                            log.debug('couldn\'t find free resource from openurl');
                            reject(reason);
                        });
                }
            }
        });
    }

    function _redirect(pURL, cto, head = true, efetch = true, specialHeaders = {}, free = false ) {
        if ( pURL === undefined || pURL == '' ) {
            return;
        }

        var url = require('url');

        if ( false === /^http(s)?\:\/\//g.test(pURL) ) {
            log.debug('False protocol test for ', pURL);
            pURL = conf.url.servicePage+'?'+pURL;
        }

        let useProxy = reProxy.test(pURL);
        let myURL = url.parse(pURL);
        let myURLwithoutProxy = url.parse(pURL.replace(reProxy,""));


        // Check the status code before redirect
        if ( false === conf.redirectSafe.some(function (e) {
            return myURL.hostname.endsWith(e) || myURLwithoutProxy.hostname.endsWith(e);
        }) ) {
            var errorpage = require('./error_page.js'),
                //cto_helper = require('./helpers/cto.js'),
                my_cto = cto_helper.my_cto(cto);

            if (head) {
                log.debug('let\'s do a HEAD request to check the URL');

                _check_URL(myURLwithoutProxy, specialHeaders, free)
                    .then(function(response) {
                        // Everything is allright and we redirect the user
                        log.debug('status of URL check:', response.statusCode);

                        if ( [302,301].includes(response.statusCode) ) {
                            if ( myURLwithoutProxy.pathname.includes('openurl') || myURLwithoutProxy.hostname.includes('openurl') || response.headers.location.includes('login') ) {
                                log.debug(`OpenURL gave ${response.statusCode}`);

                                let location = myURL.href;


                                console.log(`redirect to ${location}`);
                                _redirect(location, cto, false);
                            }
                            else if ( response.headers.location.includes('cookieSet') || myURLwithoutProxy.href == response.headers.location  ) {
                                log.debug('redirecting to page that requires cookies or redirects to itself and therefore cannot be checked');

                                res.writeHead(302, {
                                    'Location': myURL.href
                                });
                                res.end();
                            }
                            else {
                                if ( /^https?:\/\/.*/i.test(response.headers.location) ) {
                                    let useProxy = true; // We don't have this knowledge
                                    let location = (useProxy && free == 0) ? conf.url.proxyPrefix + response.headers.location : response.headers.location;
                                    console.log(`redirecting to ${location}, ${free}`);
                                    _redirect(location,cto);
                                }
                                else {
                                    _redirect(myURL.href, cto, false);
                                }
                            }
                        }
                        else {
                            log.debug('redirecting after successful URL check');

                            // Use qurl if EzProxy
                            if ( myURL.query && myURL.query.startsWith('url=') ) {
                                const q = myURL.query.substring(4);
                                myURL.href = `${myURL.protocol}//${myURL.host}${myURL.pathname}?qurl=${encodeURIComponent(q)}`;
                                if ( myURL.hash ) {
                                    myURL.href += `${encodeURIComponent(myURL.hash)}`;
                                }
                            }

                            let location = free == 1 ? myURLwithoutProxy.href : myURL.href;
                            
                            res.writeHead(302, {
                                'Location': location
                            });
                            res.end();
                        }
                    }, function(reqHead) {
                        log.debug('url check was not successful');

                        if (reqHead.aborted) {
                            log.debug('head request was aborted because of error, take a chance');

                            res.writeHead(302, {
                                'Location': myURL.href
                            });
                            res.end();
                        }
                        else if (reqHead instanceof Error) {
                            if ('Error: unable to verify the first certificate' === reqHead.toString()) {
                                log.debug('Unable to verify the first certificate but redirecting anyway'); 
                                res.writeHead(302, {
                                    'Location': myURL.href
                                });
                                res.end();
                            }
                            else {
                                log.debug(reqHead.toString());

                                if (services.find(uresolver.service.is_free)) {
                                    log.debug('The falsy URL comes from a free service in Alma. Trying to get the correct OA location');

                                    let get_oa = async function() {
                                        let doi_status = await crossref.doi_status(my_cto.doi);

                                        let par1, par2;

                                        if (doi_status == 200) {
                                            par1 = null;
                                            par2 = my_cto.doi;
                                        }
                                        else {
                                            par1 = cto_helper.to_openurl(my_cto);
                                            par2 = null;
                                        }

                                        _find_oa(par1, par2)
                                            .then(function(best_location) {
                                                _redirect(best_location);
                                            }, function(reason) {
                                                log.debug(reason);
                                                errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                            });
                                    };

                                    get_oa();

                                }
                                else {
                                    log.debug('Building error page');
                                    errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                }
                            }
                        }
                        else {
                            if ( Object.keys(my_cto).length > 0 && my_cto.constructor === Object ) {
                                var my_openurl = cto_helper.cto_to_openurl(my_cto);

                                var handleIncorrectDOI = function (cto) {
                                    crossref.get_doi( cto_helper.to_openurl(cto) )
                                        .then(function(doi) {
                                            if ( doi != cto.doi ) {
                                                log.debug('correct doi is ',doi);
                                                handleCorrectDOI(doi);
                                            }
                                            else {
                                                log.debug('Got the same DOI from Crossref, trying to get resource url candidates');
                                                crossref.get_resource_url_candidates( cto_helper.to_openurl(cto) )
                                                    .then(function(candidates) {
                                                        candidates.forEach(candidate => {
                                                            if ( false === candidate.includes( cto.doi ) ) {
                                                                _redirect(conf.url.proxyPrefix+candidate, cto);
                                                            }
                                                        });

                                                    }, function(reason) {
                                                        log.debug(reason);
                                                        console.log(my_cto);
                                                        errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                                    });

                                            }
                                        }, function(reason) {
                                            log.debug(reason);
                                            errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                        });
                                };

                                var handleCorrectDOI = function (doi) {
                                    crossref.get_resource_url_from_doi(doi)
                                        .then(function(resource_url) {
                                            let parsedResourceURL = url.parse(resource_url),
                                                our_doi = our_services.findIndex(service => service.key.parse_parameters.includes(parsedResourceURL.host));
                                            
                                            if (resource_url.includes(reqHead.getHeaders().host) || our_doi > -1) {
                                                log.debug('url from doi is on the same host so we redirect');
                                                _redirect(conf.url.proxyPrefix+resource_url, cto);
                                            }
                                            else {
                                                log.debug('the DOI is ok but not ours, so we check for a free resource');
                                                
                                                _find_oa(null, doi)
                                                    .then(function(myUrl) {
                                                        log.debug('got an oa url');
                                                        _check_URL(myUrl)
                                                            .then(function(res) {
                                                                log.debug('redirect to oa url after check');
                                                                _redirect(myUrl, cto, false); // No need to check the URL again. That would cause an endless loop
                                }, function(req) {
                                    log.debug(`${myUrl} did not pass the URL check`);

                                    if (hostsWithoutHEAD.includes(url.parse(myUrl).host)) {
                                        log.debug('redirecting anyway, cause the host does not support HEAD');
                                        _redirect(myUrl, cto, false);
                                    }
                                    else {
                                        errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                    }
                                });
                                                                    
                        }, function(reqHead) {
                            log.debug('doi is correct but not free and not ours');
                                                        
                            if ( parseInt(my_cto.spage) === 2 &&
                                 my_cto.spage === my_cto.epage ) {
                                        
                                log.debug('spage and epage both 2');
                                log.debug('trying to move spage to 1');
                                        
                                my_cto.spage = parseInt(my_cto.spage)-1;
                                my_cto.pages = my_cto.spage+'-'+my_cto.epage;
                                        
                                module.resolve(cto_helper.cto_to_openurl(my_cto));
                            }
                            else {
                                                            
                                if ( our_services.length > 1 ) {
                                    let index = our_services.findIndex(service => 'undefined' === typeof service.target_url),
                                        nextService = our_services.find(service => 'undefined' === typeof service.target_url);

                                    our_service = our_services.slice(index+1);

                                    log.debug('Use next service');
                                    _handle_service(nextService,module._OPEN_URL_REQUEST,cto); 
                                }
                                else {
                                    log.debug('building error page');
                                    errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                }
                            }
                        });
                         }
            }, function(message) {
                log.debug('Was not able to get resource url for doi', doi);
                log.debug(message);
                errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
            });
    };

    // --------------
    if ( reqHead.getHeaders().host.endsWith('doi.org') || /10\.\d{4,9}\/[-._;()/:A-Z0-9]+$/i.test(myURLwithoutProxy.path) ) {
        if ( myURLwithoutProxy.href.includes('link.springer.com') ) {
            log.debug('Changing case of DOI because SpringerLink is case sensitive');
            if ( /10\.\d{4,9}\/[-._;()/:A-Z0-9]+$/.test(myURLwithoutProxy.path) ) {
                let doiInLowerCase = myURL.href.replace(/.*(10\.\d{4,9}\/[-._;()/:A-Z0-9]+)$/, "$1").toLowerCase();

                _redirect(myURL.href.replace(/10\.\d{4,9}\/[-._;()/:A-Z0-9]+$/, doiInLowerCase));
            }
            else if ( /10\.\d{4,9}\/[-._;()/:a-z0-9]+$/.test(myURLwithoutProxy.path) ) {
                let doiInUpperCase = myURL.href.replace(/.*(10\.\d{4,9}\/[-._;()/:a-z0-9]+)$/, "$1").toUpperCase();
                _redirect(myURL.href.replace(/10\.\d{4,9}\/[-._;()/:a-z0-9]+$/, doiInUpperCase));
            }
        }
        else {
            log.debug('The DOI was used in linking so it is not correct');
            handleIncorrectDOI(my_cto);
        }


    }
    else {
        let find_it = async function() {
            let pmcid = null;
                                        
            if ( reqHead.getHeaders().host.endsWith('ncbi.nlm.nih.gov') && myURLwithoutProxy.path.includes('/pmc/articles') ) {
                try {
                    log.debug('Alma thinks PMC has the article, use Esearch');

                    if (my_cto.issn && my_cto.volume && my_cto.issue && my_cto.spage && my_cto.aulast ) {

                        var Esearch = require('./pubmed/esearch.js');
                        let esearch = new Esearch();
                                            
                        pmcid = await esearch.searchPmc(my_cto.issn, my_cto.volume, my_cto.issue, my_cto.spage, my_cto.aulast);
                    }
                    else {
                        throw('Not enough metadata to search PMC');
                    }
                }
                catch (error) {
                    log.debug(error);
                }
            }

            if ( /\d+/.test(pmcid) ) {
                _redirect('https://www.ncbi.nlm.nih.gov/pmc/articles/' + pmcid + '/');
            }
            else if ( my_cto.doi ) {
                log.debug("We have a DOI that have not been used, check the status of it");

                crossref.doi_status(my_cto.doi)
                    .then(function(statusCode) {
                        if ( 200 === statusCode ) {
                            log.debug('DOI is correct', statusCode);
                            handleCorrectDOI(my_cto.doi);
                        }
                        else {
                            log.debug('DOI seems to not be correct', statusCode);
                            _check_URL('https://doi.org/'+my_cto.doi)
                                .then(function(response) {
                                    log.debug('However it resolves');
                                    log.debug('Try to convert the DOI to PMID');


                                    var IdConv = require('./pubmed/idconv.js');
                                    var idconv = new IdConv(my_cto.doi);

                                    idconv.getResults()
                                        .then(function(response) {
                                            if ( "ok" === response.status && 1 === response.records.length ) {
                                                var Elink = require('./pubmed/elink.js');
                                                var elink = new Elink(response.records[0].pmid);

                                                elink.freeUrl()
                                                    .then(function(link) {
                                                        log.debug('using url from pubmed for free fulltext source');
                                                        _redirect(link, {}, true, true, {}, true);
                                                    }, function(reason) {
                                                        log.debug(reason);
                                                        elink.prUrl()
                                                            .then(function(objurl) {
                                                                let prLink = objurl.url.value;

                                                                if ( url.parse(prLink).hostname === myURLwithoutProxy.hostname ) {
                                                                    _redirect(conf.url.proxyPrefix+prLink, cto);
                                                                }
                                                                else {
                                                                    log.debug('Provider link in Pubmed does not match target url');
                                                                    errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);                                                    
                                                                }
                                                            }, function(reason) {
                                                                log.debug(reason);
                                                                errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);                                                    
                                                            });
                                                    });
                                            }
                                        }, function (reason) {
                                            console.log(reason);
                                            errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);                                                    
                                        });
                                }, function(reason) {
                                    handleIncorrectDOI(my_cto);
                                });
                        }
                    });
            }
            else if ( my_cto.pmid && !my_cto.doi && efetch ) {
                log.debug(`The link from Alma failed but we have a PMID`);
                console.log(myURLwithoutProxy.host);
                var Elink = require('./pubmed/elink.js');
                var elink = new Elink(my_cto.pmid);

                elink.freeUrl()
                    .then(function(link) {
                        log.debug('using url from pubmed for free fulltext source');
                        _redirect(link, my_cto);
                    }, function(reason) {
                        log.debug(reason);
                        elink.prUrl()
                            .then(function(objurl) {
                                let prLink = objurl.url.value;

                                if ( url.parse(prLink).hostname === myURLwithoutProxy.hostname ) {
                                    _redirect(conf.url.proxyPrefix+prLink, cto);
                                }
                                else {
                                    log.debug('Provider link in Pubmed does not match target url');
                                    errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);                                                    
                                }
                            }, function(reason) {
                                log.debug(reason);
                                crossref.get_resource_url( cto_helper.to_openurl(my_cto) )
                                    .then(function(resource_url) {
                                        log.debug(`Crossref has this URL #{resource_url}`);
                                        if ( reqHead.getHeaders().host.endsWith('doi.org') || ( reqHead.getHeaders().host === url.parse(resource_url).host ) ) {
                                            let location = useProxy ? conf.url.proxyPrefix + resource_url : resource_url;
                                            _redirect(location);
                                        }
                                        else {
                                            log.debug('resource url and host not the same');
                                            errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                        }
                                    }, function(reason) {
                                        log.debug(reason);
                                        errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                    });
                            });
                    });
            }
            else {
                let doiFromPath = myURLwithoutProxy.path.replace(/^.*(10\.\d{4,9}\/[-._;()\/:A-Z0-9]+).*$/i,"$1");
                if (doiFromPath != myURLwithoutProxy.path) {
                    log.debug('Use  DOI retrieved from path');
                    handleCorrectDOI(doiFromPath);
                }
                else {
                    log.debug("There's no DOI or PMID, but maybe we find it?");

                    log.debug('First try to find OA');
                    _find_oa(cto_helper.to_openurl(my_cto))
                        .then(function(best_location) {
                            _redirect(best_location);
                        }, function(reason) {
                            log.debug("No OA, but see if Crossref has an url");
                                                        
                            if ( false === crossref.questionInProgress ) {
				crossref.get_resource_url_candidates(cto_helper.to_openurl(my_cto))
				    .then(function(candidates) {
                                        candidates.forEach(function(candidate_url) {
                                            if (reqHead.getHeaders().host === url.parse(candidate_url).host) {
                                                let location = useProxy ? conf.url.proxyPrefix + candidate_url : candidate_url;
                                                _redirect(location);
                                            }
                                        });
				    }, function(reason) {
					log.debug(reason);

                                        if ( parseInt(my_cto.spage) === 2 &&
                                             my_cto.spage === my_cto.epage ) {
                                                        
                                            log.debug('spage and epage both 2');
                                            log.debug('trying to move spage to 1');
                                            // Any more case were we should try to change the start page?
                                        
                                            my_cto.spage = parseInt(my_cto.spage)-1;
                                            my_cto.pages = my_cto.spage+'-'+my_cto.epage;
                                        
                                            module.resolve(cto_helper.cto_to_openurl(my_cto));
                                        }
                                        else if ( my_cto.issue && my_cto.spage && my_cto.epage && my_cto.atitle && ( Number(my_cto.epage) - Number(my_cto.spage) > 3 ) ) {
                                            /* Run resolve again without issue and spage which often has false values
                                               Don't do it for articles 4 pages or shorter (they risk to be reviews)*/
                                                                    
                                            var new_cto = my_cto;
                                            delete new_cto.issue;
                                            delete new_cto.spage;
                                            delete new_cto.epage;
                                                    
                                            module.resolve(cto_helper.to_openurl(new_cto));
                                        }
                                        else {
                                            log.debug('Nothing to do but bild the error page');
                                            errorpage.build_html(context,res,cto,module._OPEN_URL_REQUEST);
                                        }
                                                                
                                    });
                            }
                            else {
                                console.log('Wait for ongoing Crossref question');
                            }
                                                        
                        });
                }

            }
        }
                                    
        find_it();
    }
}
else {
    log.debug('empty cto, redirecting to ' + myURL.href);
    log.debug('request was ' + module._OPEN_URL_REQUEST);
    res.writeHead(302, {
        'Location': myURL.href
    });
    res.end();
}
}
});
}
else {
    log.debug(`Redirect to ${myURL.href} without HEAD check. We asume it is already checked`);
    res.writeHead(302, {
        'Location': myURL.href
    });
    res.end();
}
}
else {
    log.debug('ExLibris\' domain, Redirect without any check');
    res.writeHead(302, {
        'Location': myURL.href
    });
    res.end();
}
}

function _resolve_unstable_service(service, openurl_old, cto) {
    return new Promise((resolve, reject) => {
        /* TODO:
           - Get the DOI from the CTO: JSON.parse(uresolver.CTO_to_JSON(cto))
           - Parse the CTO with
        */

        console.log( 'RESOLVE UNSTABLE SERVICE' );

        var doi = _get_doi(openurl, cto),
            parsed_cto = JSON.parse(uresolver.CTO_to_JSON(cto)),
            openurl = uresolver.CTO_to_OpenURL(cto);

        var cookie;

        console.log( service.key.parser_program );

        switch ( service.key.parser_program ) {
        case 'ELSEVIER::SCIENCE_DIRECT':
            if (conf.boycott.length > 0) {
                let boycott = conf.boycott.find(function(b) {
                    return 'ELSEVIER::SCIENCE_DIRECT' === b.parser_program;
                });

                if (boycott && parsed_cto['rft.month']) {
                    if (new Date(parsed_cto['rft.year']+'-'+parsed_cto['rft.month']) >= boycott.date) {
                        log.debug('we don\'t have access to this due to boycott');

                        if (parsed_cto['rft.doi']) {
                            log.debug('trying to find OA');
                            _find_oa(openurl, parsed_cto['rft.doi'])
                                .then(function(url) {
                                    resolve(url);
                                }, function(reason) {
                                    log.debug(reason);

                                    resolve(conf.url.illForm+'?'+openurl);
                                });
                        }
                    }
                    else {
                        resolve(service.target_url);
                    }
                }
                else {
                    resolve(service.target_url);
                }
            }
                
            break;

        case 'OVID::Journals':

            // See for example 10.1097/01.NAJ.0000334962.21371.66 why this is needed
            log.debug('parsing Ovid');
                
            var ovid = require('./parsers/ovid.js')(openurl, doi),
                ovid_target_url = ovid.get_target_url(service.target_url);

            if ( ovid_target_url ) {
                resolve(conf.url.proxyPrefix+ovid_target_url);
            }
            else {
                log.debug('missing ovid_target_url');
                resolve(conf.url.servicePage+'?'+openurl);
            }
            
            break;

        case 'OUP::OUP':
            var url = require('url');
                
            let targetUrl = service.target_url.replace(reProxy,"");
            let useProxy = reProxy.test(service.target_url);

            crossref.get_resource_url_candidates(openurl)
                .then(function(candidates) {
                    let location, parsedTargetUrl, parsedCandidateURL;

                    candidates.forEach(function(candidate_url, i, array) {
                        if (targetUrl === candidate_url) {
                            location = useProxy ? conf.url.proxyPrefix + candidate_url : candidate_url;
                            _redirect(location);
                        }
                        else {
                            parsedTargetUrl = url.parse(targetUrl);
                            parsedCandidateUrl = url.parse(candidate_url);

                            if (parsedCandidateUrl.host === parsedTargetUrl.host) {
                                location = useProxy ? conf.url.proxyPrefix + candidate_url : candidate_url;
                                _redirect(location);
                            }
                            else if (i === array.length -1) {
                                // If no candidate URL matches
                                resolve(service.target_url);
                            }
                        }
                    });

                }, function(reason) {
                    log.debug(reason);
                    resolve(service.target_url);
                });
                
            break;

        case 'TAYFRA::journal':
            // Use DOI if available
            if (doi) {
                crossref.get_resource_url(openurl)
                    .then(function(doiResource) {
                        if (doiResource.indexOf('tandfonline.com') < 0) {
                            log.debug('DOI is not for Taylor & Francis');
                            if (doiResource.indexOf('informaworld.com') > -1) {
                                log.debug('Send informaworld to T&F');
                                resolve(conf.url.proxyPrefix+'https://www.tandfonline.com/doi/abs/'+doi);

                            }
                            else {
                                if (services.length > 1) {
                                    var sIndex = 0+1;
                                    while (uresolver.service.is_our(services[sIndex]) && !uresolver.service.is_gin(services[sIndex])) {
                                        if (doiResource.indexOf(services[sIndex].key["interface_name"].toLowerCase()) > 5)
                                        {
                                            resolve(conf.url.proxyPrefix+doiResource);
                                        
                                        }
                                        sIndex = sIndex+1;
                                    }
                                }
                                resolve(service.target_url);
                            }
                        }
                        else {
                            let location = `${conf.url.proxyPrefix}https://www.tandfonline.com/doi/abs/${doi}`;
                            resolve(`${conf.url.proxyPrefix}https://www.tandfonline.com/doi/abs/${doi}`);
                        }
                    }, function(reason) {
                        log.debug(reason);
                        resolve(service.target_url);
                    });

            }
            else {
                resolve(service.target_url);
            }

            break;

        case 'EBSCO_HOST::ebsco_am':
            var title = "Harvard business review :";
            var subtitle = " the magazine of decision makers";
            if ( title+subtitle === parsed_cto['rft.title'] ) {
                subtitle = subtitle.replace(/ /g, "+");
                resolve (service.target_url.replace(subtitle, ""));
            } else {
                resolve (service.target_url);
            }
            break;
                        
        case 'Bulk::BULK':
            if ( doi && true === /\/content\/by\/year$/gi.test(service.target_url)  ) {
                resolve(conf.url.proxyPrefix+'https://doi.org/'+doi);
            }
            else {
                resolve(service.target_url);
            }

            break;
        case 'Wiley::WILEY':
            cookie = `randomizeUser=${Math.random()}`;
            _redirect(service.target_url, cto, true, true, {'Cookie': cookie});
                
            break;
        case 'AMA::JAMA':
            cookie = `JAMA_NetworkMachineID=${Math.floor(100000000000000000 + Math.random() * 900000000000000000)}`;
            _redirect(service.target_url, cto, false, true, {'Cookie': cookie});

            break;
        case 'SAGE::Journals':
            console.log( 'redirect to SAGE::Journals' );
            cookie = `timezone=120`;
            _redirect(service.target_url, cto, true, true, {'Cookie': cookie});
                
            break;

        case 'JSTOR::JSTOR':
            let objCTO = JSON.parse(uresolver.CTO_to_JSON(cto));

            if ( service.key.request_source && 0 === service.key.request_source.indexOf('jstor') ) {
                let jstor_= parsed_cto['rft_dat'].replace(/.*<jstor(_[a-z]*)?>(\d*)<\/jstor(_[a-z]*)?>.*/,"$2");

                _redirect(conf.url.proxyPrefix+'https://www.jstor.org/stable/'+jstor_, cto, false);
            }
            else {
                _redirect(service.target_url.replace(/issn=\d{8}/,'issn='+parsed_cto['rft.issn']));
            }
                
            break;

        default:
            switch (service.key.package_internal_name) {
            case 'COCHRANE_LIBRARY':
                var my_cto = cto_helper.my_cto(cto);

                if (doi) {
                    resolve(conf.url.proxyPrefix+'https://www.cochranelibrary.com/cdsr/doi/'+doi);
                }
                else if (my_cto.pmid) {
                    var Elink = require('./pubmed/elink.js');
                    let elink = new Elink(my_cto.pmid);

                    elink.prUrl()
                        .then(function(objurl) {
                            let prLink = objurl.url.value;
                            resolve(conf.url.proxyPrefix+prLink);
                        }, function(reason) {
                            log.debug(reason);
                            resolve(service.target_url);
                        });

                }
                else {
                    resolve(service.target_url);
                }
                break;
            default:
                resolve(service.target_url);
            }
        }
    });
}

function _resolve_service(service,openurl,cto,head,efetch) {
    var resolution;

    /* Some parser programs needs to be adjusted */
    var unstable_parser = ["Bulk::BULK",
                           "CCC::GIN",
                           "OVID::Journals",
                           "OUP::OUP",
                           "TAYFRA::journal",
                           "EBSCO_HOST::ebsco_am",
                           "Wiley::WILEY",
                           "SAGE::Journals",
                           "AMA::JAMA",
                          ];
    var unstable_package = ["COCHRANE_LIBRARY"];

    if (conf.boycott) {
        conf.boycott.forEach(function (element) {
            unstable_parser.push(element.parser_program);
        });
    }

    if ( unstable_parser.includes(service.key.parser_program) || unstable_package.includes(service.key.package_internal_name) ) {

        if ( unstable_parser.includes(service.key.parser_program) ) {
            log.debug('get resolution for '+service.key.parser_program);
            if ( 'OVID::Journals' == service.key.parser_program ) {
                head = false;
            }
        }
        else if ( unstable_package.includes(service.key.package_internal_name) ) {
            log.debug('get resolution for '+service.key.package_internal_name);
        }
        
        resolution = _resolve_unstable_service(service, openurl, cto, head);
    }
    else {
        log.debug("resolve to uresolver's target url for " + service.key.parser_program);
        resolution = Promise.resolve(service.target_url);
    }

    resolution.then(function(url) {
        log.debug('resolved resolution promise');
        _redirect(url, cto, head, efetch, {}, service.key.Is_free);
    }, function(reason) {
        log.debug(reason);
        log.debug('redirecting to service page');
        _redirect(openurl);
    });
}

function _check_URL(urlToCheck, specialHeaders = {}) {
    return new Promise((resolve, reject) => {
        var url = require('url'),
            http;

        var myURL = url.parse(urlToCheck);
        var options = {method: 'HEAD', host: myURL.hostname, path: myURL.path};

        switch (myURL.protocol) {
        case 'https:':
            http = require('https');
            break;
        case 'http:':
            http = require('http');
            break;
        default:
            break;
        }

        if (JSON.stringify(specialHeaders) !== JSON.stringify({}) ) {
            options.headers = specialHeaders;
        }
        /*else if ( myURL.host.endsWith('wiley.com') ) {
          let cookie = `randomizeUser=${Math.random()}`;
          options.headers = {'Cookie': cookie};
          }*/
                  
        log.debug(`Checking ${myURL.href}`);
        log.debug(options);


        var req = http.request(options, function(res) {
            if ( ( res.statusCode < 200 || res.statusCode > 399 ) && false === [403,405,503].includes(res.statusCode) ) {
                log.debug('HEAD request gave', res.statusCode);
                reject(req);
            }
            else if ( [403, 405, 503].includes(res.statusCode) ) {
                log.debug(`HEAD request gave ${res.statusCode}, try GET`);
 
                let get_req = http.request({method: 'GET', headers: {'User-Agent': conf.userAgent}, host: options.host, path: options.host}, function(get_res) {
                    let data = '';

                    res.on('data', (chunk) => {
                        data+= chunk;
                    });
                            
                    res.on('end', () => {
                        log.debug(`GET gave ${res.statusCode}`);
                                
                        if ( ( res.statusCode < 200 || res.statusCode > 399 ) && false === [403,405,503].includes(res.statusCode) ) {
                            reject(req);
                        }
                        else {
                            resolve(res);
                        }
                    });
                }).on('error', (err) => {
                    console.log(err.message);
                });

                get_req.end();
            }
            else {
                log.debug('HEAD request gave', res.statusCode); // t.ex. 301
                resolve(res);
            }
        });

        req.setTimeout(1500, function() {
            log.debug('timeout on head request');
            reject(req);
            req.abort();
        });

        req.on("error", (err) => {
            console.log("Error: " + err.message);
            reject(err);
        });

        req.end();
    });
}
//---

return module;

};

