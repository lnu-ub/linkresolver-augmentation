/*************************************************
 * linkresolver augmenation                      
 *
 * Hämtar metadata från pubmed samt fixar        
 * egen länkning (post augumentation) i de fall
 * där uresolver ger ett bristfälligt resultat
 *************************************************/
const bunyan = require('bunyan'),
      log = bunyan.createLogger({
          name: 'linkresolver-augmentation',
          streams: [
              {
                  level: 'info',
                  stream: process.stdout
              },
              {
                  level: 'error',
                  stream: process.stderr
              }
          ]
      });

const http = require('http'),
      querystring = require('querystring');
/*************************************************/


// main
const server = http.createServer(function (req, res) {

    if ( req.url.indexOf('/openurl?') === 0) {
        try {
            var q = querystring.parse( req.url.substring( req.url.indexOf('?')+1 ) ),
                context,
                pmid;

            if ( "string" === typeof q.context ) {
                context = q.context ? q.context : 'default';
            }
            else if ( Array.isArray(q.context) ) {
                // choose the first if we have several contexts
                context = q.context[0];
                q.context.forEach(function(value) {
                    if ( value != context ) {
                        log.debug('context set to '+context+' but '+value+'is another possible context');
                    }
                });
            }
            else {
                context = 'default';
            }

            var completion = require('./completion.js')(req, res, context);

            // Get identifiers (e.g. PMID)
            if ( q.pmid ) {
                pmid = q.pmid;
            }
            else if ( q.id ) {
                if ( Array.isArray(q.id) ) {
                    q.id.forEach(function(value, i) {
                        if ( value.startsWith('pmid:') ) {
                            pmid = value.match(/\d*$/);
                        }
                    });
                }
                else if ( /pmid:\d*/ig.test(q.id) ) {
                    pmid = q.id.match(/\d*$/);
                }
            }
            else if ( 1 == Object.keys(q).length ) {
                let cleanQ = {};
                if ( /^\d{7,8}$/i.test(Object.keys(q)[0]) ) {
                    pmid = Object.keys(q)[0];
                    cleanQ.id = `pmid:${pmid}`;
                }
                else if ( /^10.\d{4,9}\/[-._;()\/:A-Z0-9]+$/i.test(Object.keys(q)[0]) ) {
                    cleanQ.id = `doi:${Object.keys(q)[0]}`;
                }

                if ( 1 == Object.keys(cleanQ).length ) {
                    q = cleanQ;
                }
            }            

            // Use Pubmed if possible
            if ( Object.keys(q).length > 0 && q ) {
                var openurl = querystring.stringify(q);
            }
            
            if ( pmid || true === /^\d*\d$/g.test(pmid) ) {
                var pubmed = require('./pubmed.js')(req, res, context);
                pubmed.request(pmid, openurl);
            }
            else if ( openurl ) {
                completion.resolve( openurl );
            }
            else {
                res.statusCode = 400;
                res.end();
            }
            
        }
        catch (err) {
            console.log(err.code);
            log.error(err);
        }
        
    }
    else {
        res.statusCode = 400;
        res.end();
    }

});

server.listen(8008, null, function() {
});

